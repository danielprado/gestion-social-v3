<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    protected $table = "tbl_file_type";
    protected $primaryKey = 'id';
    protected $fillable = ['file_type'];
    public $timestamps = false;
}
