<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = 'tbl_proceso';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_nombre','i_estado'];
    protected $connection = '';
    public $timestamps = false;


    public function programaciones()
    {
        return $this->hasMany(Programacion::class,'i_fk_id_proceso');
    }
}
