<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commitments extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_compromisos';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['i_fk_id_programacion', 'vc_responsable', 'tx_compromiso','d_fecha', 't_hora_ini', 't_hora_fin', 'i_estado', 'tx_solucion', 'dt_solucion'];

    protected $dates = ['deleted_at'];

    public function programming()
    {
        return $this->belongsTo(Programacion::class,'i_fk_id_programacion');
    }

    public function programming_view()
    {
        return $this->belongsTo(ProgrammingView::class,'i_fk_id_programacion', 'id');
    }
}
