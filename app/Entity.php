<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $table = 'entidadessocial';
    protected $primaryKey = 'idEntidad';
    protected $fillable = ['Entidad','i_estado'];
}
