<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attemption extends Model
{
    protected $table = 'tbl_attention';
    protected $primaryKey = 'id';
    protected $fillable = ['journey','start_hour','end_hour'];
    public $timestamps = false;
}
