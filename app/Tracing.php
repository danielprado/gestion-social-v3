<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracing extends Model
{
    protected $table = 'agreements_2018';

    public $timestamps = false;
}
