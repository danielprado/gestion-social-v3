<?php

namespace App;

use Idrd\Parques\Repo\Parque;
use Illuminate\Database\Eloquent\Model;

class ProgrammingView extends Model
{
    protected $table = 'programacion_view';

    public function process()
    {
        return $this->belongsTo(Process::class,'process');
    }

    public function request()
    {
        return $this->belongsTo(Request::class,'request');
    }

    public function who_calls()
    {
        return $this->belongsTo(QuienConvoca::class,'who_calls');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class,'activity');
    }

    public function park()
    {
        return $this->belongsTo(Parque::class,'park');
    }

    public function person()
    {
        return $this->belongsTo(Persona::class,'user');
    }

    public function commitments()
    {
        return $this->hasMany(Commitments::class,'i_fk_id_programacion');
    }

    public function execution()
    {
        return $this->hasMany(ExecutionView::class,'programming_id');
    }

    public function agreements()
    {
        return $this->hasMany(Acta::class,'i_fk_id_programacion', 'id');
    }

    public function tracing()
    {
        return $this->hasMany( Tracing::class, 'park_code', 'park_code' );
    }
}
