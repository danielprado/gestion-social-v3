<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Execution extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ejecucion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'i_fk_id_programacion',
        'i_fk_id_entidad',
        'i_fk_id_tipo',
        'i_fk_id_condicion',
        'i_fk_id_situacion',
        'i_0a5_f',
        'i_0a5_m',
        'i_6a12_f',
        'i_6a12_m',
        'i_13a17_f',
        'i_13a17_m',
        'i_18a26_f',
        'i_18a26_m',
        'i_27a59_f',
        'i_27a59_m',
        'i_60_f',
        'i_60_m'
    ];

    public function entity()
    {
        return $this->belongsTo(Entity::class, 'i_fk_id_entidad');
    }

    public function population_type()
    {
        return $this->belongsTo(PopulationType::class, 'i_fk_id_tipo');
    }

    public function condition()
    {
        return $this->belongsTo(Condition::class, 'i_fk_id_condicion');
    }

    public function situation()
    {
        return $this->belongsTo(Situation::class, 'i_fk_id_situacion');
    }

    public function programming()
    {
        return $this->belongsTo(Programacion::class,'i_fk_id_programacion');
    }
}
