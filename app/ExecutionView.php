<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecutionView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'execution_view';

    public function programming_view()
    {
        return $this->belongsTo(ProgrammingView::class,'programming_id');
    }
}
