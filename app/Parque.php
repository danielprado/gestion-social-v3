<?php


namespace App;


class Parque extends \Idrd\Parques\Repo\Parque
{
    protected $table = 'parque';
    protected $primaryKey = 'Id';
    protected $connection = '';
    public $timestamps = false;

    public function __construct()
    {
        $this->connection = config('parques.conexion');
    }

}