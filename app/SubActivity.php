<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubActivity extends Model
{
    protected $table = 'tbl_subactivity';
    protected $primaryKey = 'id';
    protected $fillable = ['sub_activity','activity_id'];
    public $timestamps = false;
}
