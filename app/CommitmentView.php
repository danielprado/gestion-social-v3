<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommitmentView extends Model
{
    protected $table = 'commitment_view';
}
