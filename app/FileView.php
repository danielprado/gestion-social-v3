<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileView extends Model
{
    protected $table = 'files_view';
}
