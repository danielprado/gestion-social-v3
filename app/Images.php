<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'imagenesejecucion';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['i_fk_id_programacion','tx_descripcion', 'bl_is_related', 'vc_file_name', 'vc_path'];
}
