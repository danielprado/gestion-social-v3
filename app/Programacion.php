<?php

namespace App;

use Idrd\Parques\Repo\Parque;
use Illuminate\Database\Eloquent\Model;

class Programacion extends Model
{
    //
    protected $table = 'tbl_programacion';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = [
        'd_fecha',
        't_hora_inicio',
        't_hora_fin',
        'i_fk_reunion_type',
        'i_fk_attention',
        'i_fk_id_parque',
        'vc_lugar',
        'tx_objetivo',
        'i_fk_id_quien_convoca',
        'vc_quien_convoca',
        'i_fk_id_proceso',
        'i_fk_id_solicitud',
        'i_fk_id_actividad',
        'i_fk_id_subactividad',
	    'i_fk_id_usuario',
        'i_estado',
    ];
	protected $connection = '';
	public $timestamps = true;

    public function process()
    {
        return $this->belongsTo(Process::class,'i_fk_id_proceso');
    }

    public function request()
    {
        return $this->belongsTo(Request::class,'i_fk_id_solicitud');
    }

    public function who_calls()
    {
        return $this->belongsTo(QuienConvoca::class,'i_fk_id_solicitud');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class,'i_fk_id_actividad');
    }

    public function park()
    {
        return $this->belongsTo(Parque::class,'i_fk_id_parque');
    }

    public function person()
    {
        return $this->belongsTo(Persona::class,'i_fk_id_usuario');
    }

    public function commitments()
    {
        return $this->hasMany(Commitments::class,'i_fk_id_programacion');
    }

    public function execution()
    {
        return $this->hasMany(Execution::class,'i_fk_id_programacion');
    }

}
