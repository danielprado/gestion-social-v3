<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = 'tipopoblaciones';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_tipo','i_estado'];
}
