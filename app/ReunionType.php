<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class ReunionType extends Model
{
    protected $table = 'tbl_reunion_type';
    protected $primaryKey = 'id';
    protected $fillable = ['reunion_type','journey','start_time', 'end_time'];
    public $timestamps = false;
}