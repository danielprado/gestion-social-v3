<?php

namespace App;

use App\Programacion;
use Illuminate\Database\Eloquent\Model;

class QuienConvoca extends Model
{
    //
    protected $table = 'tbl_quien_convoca';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_nombre','i_estado'];
	protected $connection = ''; 
	public $timestamps = false;

	protected $casts = [
	    'has_input' =>  'boolean'
    ];

	public function programaciones()
    {
        return $this->hasMany(Programacion::class,'i_fk_id_solicitud');
    }
}
