<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model
{
    protected $table = 'situaciones';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_situacion','i_estado'];
}
