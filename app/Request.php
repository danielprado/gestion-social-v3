<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $table = 'tbl_solicitud';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_nombre','i_estado'];
    protected $connection = '';
    public $timestamps = false;


    public function programaciones()
    {
        return $this->hasMany(Programacion::class,'i_fk_id_solicitud');
    }
}
