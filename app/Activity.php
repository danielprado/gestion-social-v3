<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'tbl_actividad';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_nombre','i_estado','i_fk_id_proceso'];
    protected $connection = '';
    public $timestamps = false;


    public function programaciones()
    {
        return $this->hasMany(Programacion::class,'i_fk_id_actividad');
    }
}
