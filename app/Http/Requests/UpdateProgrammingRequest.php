<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProgrammingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isset( $_SESSION['Usuario']['Permisos']['create_schedules'] )
        && (int) $_SESSION['Usuario']['Permisos']['create_schedules'] == 1
            ? true
            : false;
    }

    /**
     * Get the response for a forbidden operation.
     *
     * @return \Illuminate\Http\Response
     */
    public function forbiddenResponse()
    {
        return request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'execution_date'  =>  'required|date',
            'initial_time'  =>  'required|date_format:H:i',
            'end_time'  =>  'required|date_format:H:i',
        ];
    }
}
