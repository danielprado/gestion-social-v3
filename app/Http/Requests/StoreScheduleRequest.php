<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreScheduleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isset( $_SESSION['Usuario']['Permisos']['create_schedules'] )
        && (int) $_SESSION['Usuario']['Permisos']['create_schedules'] == 1
            ? true
            : false;
    }

    /**
     * Get the response for a forbidden operation.
     *
     * @return \Illuminate\Http\Response
     */
    public function forbiddenResponse()
    {
        return request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'execution_date'  =>  'required|date',
            'initial_time'  =>  'required|date_format:H:i',
            'end_time'  =>  'required|date_format:H:i',
            'type_of_meeting'  =>  'required_if:attemption,null',
            'attemption'  =>  'required_if:type_of_meeting,null',
            'park'  =>  'required|numeric',
            'place'  =>  'required|string|min:2|max:255',
            'objective'  =>  'required|string|min:6|max:2500',
            'who_calls'  =>  'required|numeric',
            'who_calls_text'  =>  'required_if:who_calls,6,7,12|string|max:255',
            'request'  =>  'required|numeric',
            'process'  =>  'required|numeric',
            'activity'  =>  'required|numeric',
            'subactivity'  =>  'required_if:activity,6,32|numeric',
        ];
    }
}
