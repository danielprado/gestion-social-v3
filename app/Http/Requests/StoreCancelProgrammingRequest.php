<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreCancelProgrammingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isset( $_SESSION['Usuario']['Permisos']['create_schedules'] )
        && (int) $_SESSION['Usuario']['Permisos']['create_schedules'] == 1
            ? true
            : false;
    }

    public function forbiddenResponse()
    {
        return request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'who_cancel'    =>  'required|string|max:100|min:6',
            'text_cancel'   =>  'required|string|max:2500|min:6'
        ];
    }
}
