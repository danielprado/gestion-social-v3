<?php
session_start();
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@welcome')->name('welcome');
Route::any('/welcome', 'MainController@index');
Route::post('login', 'MainController@login');
Route::any('/logout', 'MainController@logout')->name('logout');
//rutas con filtro de autenticación
Route::group(['middleware' => ['check.auth']], function () {
    Route::post('api/user/check', 'MainController@check');
    Route::group(['prefix'  => 'api'], function () {
        Route::get('user', 'MainController@auth');

        Route::get('parks/finder', 'Parques\ParquesController@finder');
        Route::get('parks/programmed', 'Parques\ParquesController@programmed');
        Route::resource('parks', 'Parques\ParquesController', [
            'only'  =>  ['index']
        ]);
        Route::resource('reunion-types', 'ReunionType\ReunionTypeController', [
            'only'  =>  ['index']
        ]);
        Route::resource('who-calls', 'WhoCalls\WhoCallsController', [
            'only'  =>  ['index']
        ]);
        Route::resource('request', 'Request\RequestController', [
            'only'  =>  ['index']
        ]);
        Route::resource('process', 'Process\ProcessController', [
            'only'  =>  ['index']
        ]);
        Route::resource('activities', 'Activity\ActivityController', [
            'only'  =>  ['show']
        ]);
        Route::resource('entities', 'Entity\EntityController', [
            'only'  =>  ['show']
        ]);
        Route::resource('attemption', 'Attemption\AttemptionController', [
            'only'  =>  ['index']
        ]);
        Route::resource('subactivities', 'SubActivity\SubActivityController', [
            'only'  =>  ['show']
        ]);
        Route::resource('entities', 'Entity\EntityController', [
            'only'  =>  ['index']
        ]);
        Route::resource('types', 'Type\TypeController', [
            'only'  =>  ['index']
        ]);
        Route::resource('conditions', 'Condition\ConditionController', [
            'only'  =>  ['index']
        ]);
        Route::resource('situations', 'Situation\SituationController', [
            'only'  =>  ['index']
        ]);
        Route::resource('file-types', 'FileType\FileTypeController', [
            'only'  =>  ['index']
        ]);
        Route::resource('professionals', 'Professionals\ProfessionalController', [
            'only'  =>  ['index']
        ]);
        Route::post('report', 'Report\ReportController@index');

        Route::get('programming/counters', 'Programming\ProgrammingController@counters');
        Route::get('programming/calendar', 'Programming\ProgrammingController@calendar');
        Route::post('programming/{id}/cancel', 'Programming\ProgrammingController@cancel');
        Route::get('commitments/calendar', 'Programming\ProgrammingController@eventsCommitments');
        Route::resource('programming', 'Programming\ProgrammingController', [
            'except'  =>  ['create', 'edit', 'destroy']
        ]);
        Route::resource('programming.execution', 'ProgrammingExecution\ProgrammingExecutionController', [
            'only'  =>  ['index', 'store']
        ]);
        Route::delete('programming/{id}/file/delete', 'ProgrammingExecution\ProgrammingExecutionController@deleteFile');
        Route::post('commitment/{commitment}/solution', 'ProgrammingExecution\ProgrammingExecutionController@storeSolution');
        Route::get('programming/{programming}/file', 'ProgrammingExecution\ProgrammingExecutionController@file');
        Route::get('programming/{programming}/images', 'ProgrammingExecution\ProgrammingExecutionController@images');
        Route::get('programming/{programming}/commitments', 'ProgrammingExecution\ProgrammingExecutionController@commitments');
    });

});
