<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( isset( $_SESSION['auth'] ) && isset($_SESSION['Usuario']) ) {
            return $next($request);
        }

        if ( $request->ajax() && $request->wantsJson() ) {
            return response()->json([
                'message'   => trans('validation.handler.unauthenticated'),
                'code'      => 401
            ], 401);
        }

        return redirect()->route('logout');
    }
}
