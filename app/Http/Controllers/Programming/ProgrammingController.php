<?php

namespace App\Http\Controllers\Programming;

use App\Activity;
use App\Attemption;
use App\Commitments;
use App\Http\Controllers\ReunionType\ReunionTypeController;
use App\Process;
use App\Programacion;
use App\ProgrammingView;
use App\QuienConvoca;
use App\ReunionType;
use App\SubActivity;
use App\Transformers\Activity\ActivityTransformer;
use App\Transformers\Commitments\CommitmentCalendarTransformer;
use App\Transformers\Process\ProcessTransformer;
use App\Transformers\Programming\ProgrammingCalendarTransformer;
use App\Transformers\Programming\ProgrammingTransformer;
use App\Transformers\Request\RequestTransformer;
use App\Transformers\ReunionType\ReunionTypeTransformer;
use App\Transformers\SubActivity\SubActivityTransformer;
use App\Transformers\WhoCalls\WhoCallsTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ProgrammingController extends Controller
{

    public function counters()
    {
        return \response()->json([
            'data'  => [
                'total'         =>  ProgrammingView::query()
                                        ->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                            return $q->where('user', $_SESSION['Id_funcionario']);
                                        })->where('status', true)->count(),
                'no_execution'  =>  ProgrammingView::query()
                                        ->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                            return $q->where('user', $_SESSION['Id_funcionario']);
                                        })->doesntHave('execution')->where('status', true)->count(),
                'no_agreements' =>  ProgrammingView::query()
                                        ->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                            return $q->where('user', $_SESSION['Id_funcionario']);
                                        })->doesntHave('agreements')->where('status', true)->count(),
                'canceled'      =>   ProgrammingView::query()
                                        ->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                            return $q->where('user', $_SESSION['Id_funcionario']);
                                        })->where('status', false)->count(),
                'commitments'   =>   Commitments::whereHas('programming_view', function ($query) {
                                                        return $query->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                                            return $q->where('user', $_SESSION['Id_funcionario']);
                                                        })->where('status', true);
                                                    })->count(),
                'no_solved'     =>   Commitments::whereHas('programming_view', function ($query) {
                                                    return $query->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                                        return $q->where('user', $_SESSION['Id_funcionario']);
                                                    })->where('status', true);
                                                })->where('i_estado', false)->count(),
                'solved'         =>   Commitments::whereHas('programming_view', function ($query) {
                                                    return $query->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                                        return $q->where('user', $_SESSION['Id_funcionario']);
                                                    })->where('status', true);
                                                })->where('i_estado', true)->count(),
                'tracing'        =>  ProgrammingView::query()
                                                    ->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                                                        return $q->where('user', $_SESSION['Id_funcionario']);
                                                    })->has('tracing')->where('status', true)->count(),
            ],
            'code'  => 200
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'created_at';
        $order = \request()->has( 'order' ) ? \request()->get('order') : 'desc';
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;
        $status = \request()->has('canceled') ? ! \request()->get('canceled') : true;

        $order =  ( $order == 'asc' ) ? $order : 'desc';

        $programming = ProgrammingView::query();

        if ( $query ) {
            $programming = $programming->where( function ( $q ) use ($query) {
                  $q->where('id', 'LIKE', "%{$query}%")
                    ->orWhere('full_name', 'LIKE', "%{$query}%")
                    ->orWhere('execution_date', 'LIKE', "%{$query}%")
                    ->orWhere('initial_time', 'LIKE', "%{$query}%")
                    ->orWhere('end_time', 'LIKE', "%{$query}%")
                    ->orWhere('park_name', 'LIKE', "%{$query}%")
                    ->orWhere('park_address', 'LIKE', "%{$query}%")
                    ->orWhere('park_code', 'LIKE', "%{$query}%")
                    ->orWhere('place', 'LIKE', "%{$query}%")
                    ->orWhere('objective', 'LIKE', "%{$query}%")
                    ->orWhere('who_calls_name', 'LIKE', "%{$query}%")
                    ->orWhere('request_name', 'LIKE', "%{$query}%")
                    ->orWhere('process_name', 'LIKE', "%{$query}%")
                    ->orWhere('activity_name', 'LIKE', "%{$query}%")
                    ->orWhere('created_at', 'LIKE', "%{$query}%");
            });
        }

        if ( \request()->has('execution') ) {
            $programming = $programming->doesntHave('execution');
        }

        if ( \request()->has('agreements') ) {
            $programming = $programming->doesntHave('agreements');
        }

        $programming = $programming->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
            return $q->where('user', $_SESSION['Id_funcionario']);
        });

        $programming = $programming->where('status', $status)->orderBy( $column, $order )->paginate( $per_page );

        $resource = $programming->getCollection()
            ->map(function($model) {
                return ( new ProgrammingTransformer() )->transform( $model );
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $programming->total(),
            (int) $programming->perPage(),
            (int) $programming->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $programming->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request)
    {
        $start = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end = $request->has( 'end' ) ? $request->get('end') : Carbon::now()->endOfMonth();
        $programming = ProgrammingView::query()->select([
            'id',
            'execution_date',
            'initial_time',
            'end_time',
            'activity_name',
            'park_code',
            'park_name'
        ])->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
            return $q->where('user', $_SESSION['Id_funcionario']);
        })->where('status', true )->whereBetween( 'execution_date', [$start, $end] )->get();

        $resource = new Collection($programming, new ProgrammingCalendarTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray()['data'], 200 );
    }

    public function eventsCommitments(Request $request)
    {
        $start = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end = $request->has( 'end' ) ? $request->get('end') : Carbon::now()->endOfMonth();
        $commitments = Commitments::query()->whereHas('programming_view', function ($q) {
            return $q->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
                            return $q->where('user', $_SESSION['Id_funcionario']);
                        })->where('status', true);
        })->select([
            'i_pk_id',
            'i_fk_id_programacion',
            'd_fecha',
            't_hora_ini',
            't_hora_fin',
            'created_at',
        ])->whereBetween( 'd_fecha', [$start, $end] )->get();
        $resource = new Collection($commitments, new CommitmentCalendarTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray()['data'], 200 );
    }


    /**
     * Display a listing of the selectors.
     *
     * @return Response
     */
    public function selectionsData()
    {
        $manager = new Manager();

        $reunion = ReunionType::all();
        $reunion = new Collection($reunion, new ReunionTypeTransformer);
        $reunion = $manager->createData($reunion);

        $who = QuienConvoca::all();
        $who = new Collection($who, new WhoCallsTransformer );
        $who = $manager->createData($who);

        $request = \App\Request::all();
        $request = new Collection($request, new RequestTransformer() );
        $request = $manager->createData($request);

        $process = Process::all();
        $process = new Collection($process, new ProcessTransformer() );
        $process = $manager->createData($process);

        $activities = Activity::all();
        $activities = new Collection($activities, new ActivityTransformer() );
        $activities = $manager->createData($activities);

        $subactivities = SubActivity::all();
        $subactivities = new Collection($subactivities, new SubActivityTransformer() );
        $subactivities = $manager->createData($subactivities);

        $array = [
            'reunion_type'  =>  $reunion->toArray()['data'],
            'who_calls'     =>  $who->toArray()['data'],
            'request'       =>  $request->toArray()['data'],
            'process'       =>  $process->toArray()['data'],
            'activities'    =>  $activities->toArray()['data'],
            'subactivities' =>  $subactivities->toArray()['data'],

        ];


        return response()->json([
            'data'  =>  $array,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Requests\StoreScheduleRequest $request)
    {

        $start_week = Carbon::parse( $request->get('execution_date') )->startOfWeek()->format('Y-m-d');
        $end_week = Carbon::parse( $request->get('execution_date') )->endOfWeek()->format('Y-m-d');

        if ( $request->has('attemption') ) {
            $attention = ProgrammingView::query()->where('status', true)
                ->whereBetween('execution_date', [ $start_week, $end_week ])
                ->where('user', $_SESSION['Id_funcionario'] )
                ->where('attention', $request->get('attemption'))
                ->count();

            if ( $attention > 1 ) {
                return response()->json([
                    'message'  => trans('validation.handler.attention'),
                    'code'  => 422
                ], 422);
            }
        } else {
            $schedules = ProgrammingView::query()->where('execution_date', $request->get('execution_date'))
                                                 ->where('status', true)
                                                 ->where('user', $_SESSION['Id_funcionario'] )->get()->toArray();

            $start_init = Carbon::parse( "{$request->get('execution_date')} {$request->get('initial_time')}");
            $start_end  = Carbon::parse( "{$request->get('execution_date')} {$request->get('end_time')}");

            $created = array_where( $schedules, function ( $key, $value ) use ( $start_init, $start_end ) {
                $created_init = Carbon::parse( "{$value['execution_date']} {$value['initial_time']}");
                $created_end  = Carbon::parse( "{$value['execution_date']} {$value['end_time']}");
                return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
            });

            if ( count( $created ) > 0 ) {
                return response()->json([
                    'message'  => trans('validation.handler.exist'),
                    'code'  => 422
                ], 422);
            }

            $commitments = Commitments::query()->where('d_fecha', $request->get('execution_date'))
                                               ->whereHas('programming_view', function ($query) {
                                                   return $query->where('user', $_SESSION['Id_funcionario'] )->where('status', true);
                                               })->get()->toArray();

            $start_init = Carbon::parse( "{$request->get('execution_date')} {$request->get('initial_time')}");
            $start_end  = Carbon::parse( "{$request->get('execution_date')} {$request->get('end_time')}");

            $created = array_where( $commitments, function ( $key, $value ) use ( $start_init, $start_end ) {
                $created_init = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_ini']}");
                $created_end  = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_fin']}");
                return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
            });

            if ( count( $created ) > 0 ) {
                return response()->json([
                    'message'  => trans('validation.handler.commit_exist'),
                    'code'  => 422
                ], 422);
            }
        }

        $schedule = new Programacion();
        $schedule->d_fecha = Carbon::parse( $request->get('execution_date') )->format('Y-m-d');
        $schedule->t_hora_inicio = $request->get('initial_time');
        $schedule->t_hora_fin = $request->get('end_time');
        $schedule->i_fk_reunion_type = $request->get('type_of_meeting');
        $schedule->i_fk_attention = $request->get('attemption');
        $schedule->i_fk_id_parque = $request->get('park');
        $schedule->vc_lugar = $request->get('place');
        $schedule->tx_objetivo = $request->get('objective');
        $schedule->i_fk_id_quien_convoca = $request->get('who_calls');
        $schedule->vc_quien_convoca = $request->get('who_calls_text');
        $schedule->i_fk_id_proceso = $request->get('process');
        $schedule->i_fk_id_solicitud = $request->get('request');
        $schedule->i_fk_id_actividad = $request->get('activity');
        $schedule->i_estado = true;
        $schedule->i_fk_id_subactividad = $request->get('subactivity');
        $schedule->i_fk_id_usuario = $_SESSION['Id_funcionario'];
        $schedule->save();

        $programming = ProgrammingView::query()->whereBetween('execution_date', [ Carbon::parse( $request->get('execution_date') )->startOfMonth()->format('Y-m-d'),
                                                                                  Carbon::parse( $request->get('execution_date') )->endOfMonth()->format('Y-m-d') ]
                                                )
                                                ->where('park', $request->get('park'))
                                                ->count();


        if ( $programming > 1 ) {
            return response()->json([
                'data'  => trans('validation.handler.park', ['num' => $programming]),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'data'  => trans('validation.handler.success'),
            'code'  => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $programming = ProgrammingView::query()->where('id', $id)->first();

        if ( $programming ) {
            $resource = new Item($programming, new ProgrammingTransformer() );
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json($rootScope->toArray(), 200);
        }
        return response()->json([
            'message'  => trans('validation.handler.resource_not_found'),
            'code'  => 404
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Requests\StoreCancelProgrammingRequest $request
     * @param int $id
     * @return Response
     */
    public function cancel(Requests\StoreCancelProgrammingRequest $request, $id)
    {

        $programming = Programacion::query()->where('id', $id)->when( (int) $_SESSION['Usuario']['Permisos']['admin'] == 0, function ($q) {
            return $q->where('user', $_SESSION['Id_funcionario']);
        })->first();

        if ( $programming ) {
            $programming->i_estado = 0;
            $programming->vc_quien_cancela = $request->get('who_cancel');
            $programming->txt_cancelacion = $request->get('text_cancel');
            if ( $programming->save() ) {
                return response()->json([
                    'data'  => trans('validation.handler.success'),
                    'code'  => 200
                ], 200);
            }

        }
        return response()->json([
            'message'  => trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\UpdateProgrammingRequest $request, $id)
    {
        $schedule = Programacion::find( $id );
        $start_week = Carbon::parse( $request->get('execution_date') )->startOfWeek()->format('Y-m-d');
        $end_week = Carbon::parse( $request->get('execution_date') )->endOfWeek()->format('Y-m-d');

        if ( $schedule ) {
            if ( isset( $schedule->i_fk_attention ) ) {
                $attention = ProgrammingView::query()->where('status', true)
                    ->whereBetween('execution_date', [ $start_week, $end_week ])
                    ->where('attention', $schedule->i_fk_attention )
                    ->count();

                if ( $attention > 0 ) {
                    return response()->json([
                        'message'  => trans('validation.handler.attention'),
                        'code'  => 422
                    ], 422);
                }
            } else {

                $schedules = ProgrammingView::query()->where('execution_date', $request->get('execution_date'))
                    ->where('status', true)
                    ->where('user', $_SESSION['Id_funcionario'] )->get()->toArray();

                $start_init = Carbon::parse( "{$request->get('execution_date')} {$request->get('initial_time')}");
                $start_end  = Carbon::parse( "{$request->get('execution_date')} {$request->get('end_time')}");

                $created = array_where( $schedules, function ( $key, $value ) use ( $start_init, $start_end ) {
                    $created_init = Carbon::parse( "{$value['execution_date']} {$value['initial_time']}");
                    $created_end  = Carbon::parse( "{$value['execution_date']} {$value['end_time']}");
                    return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
                });

                if ( count( $created ) > 0 ) {
                    return response()->json([
                        'message'  => trans('validation.handler.exist'),
                        'code'  => 422
                    ], 422);
                }

                $commitments = Commitments::query()->where('d_fecha', $request->get('execution_date'))
                    ->whereHas('programming_view', function ($query) {
                        return $query->where('user', $_SESSION['Id_funcionario'] )->where('status', true);
                    })->get()->toArray();

                $start_init = Carbon::parse( "{$request->get('execution_date')} {$request->get('initial_time')}");
                $start_end  = Carbon::parse( "{$request->get('execution_date')} {$request->get('end_time')}");

                $created = array_where( $commitments, function ( $key, $value ) use ( $start_init, $start_end ) {
                    $created_init = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_ini']}");
                    $created_end  = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_fin']}");
                    return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
                });

                if ( count( $created ) > 0 ) {
                    return response()->json([
                        'message'  => trans('validation.handler.commit_exist'),
                        'code'  => 422
                    ], 422);
                }

                $schedule->d_fecha = Carbon::parse( $request->get('execution_date') )->format('Y-m-d');
                $schedule->t_hora_inicio = $request->get('initial_time');
                $schedule->t_hora_fin = $request->get('end_time');
                $schedule->save();

                return response()->json([
                    'data'  => trans('validation.handler.success'),
                    'code'  => 200
                ], 200);
            }
        }

        return response()->json([
            'data'  => trans('validation.handler.resource_not_found'),
            'code'  => 404
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
