<?php

namespace App\Http\Controllers\Condition;

use App\Condition;

use App\Http\Controllers\Controller;
use App\Transformers\Condition\ConditionTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ConditionController extends Controller
{
    public function index()
    {
        $data = Condition::all();
        $resource = new Collection($data, new ConditionTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
