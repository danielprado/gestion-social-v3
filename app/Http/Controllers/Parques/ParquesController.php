<?php

namespace App\Http\Controllers\Parques;

use App\ProgrammingView;
use App\Transformers\Parques\ParqueTransformer;
use App\Parque;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ParquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parques = Parque::paginate( 5 );

        return response()->json([
            'data'  =>  $parques,
            'code'  =>  200
        ], 200);
    }

    public function finder(Request $request)
    {
        $parks = Parque::query()->where('Id_IDRD', 'LIKE', "%{$request->get('query')}%")
                                ->orWhere('Nombre', 'LIKE', "%{$request->get('query')}%")
                                ->orWhere('Direccion', 'LIKE', "%{$request->get('query')}%")
                                ->get(['Id', 'Id_IDRD', 'Nombre', 'Direccion', 'Upz'])->filter( function ($value, $key) {
                                    return ( isset( $value->Id ) &&  $value->Id != 1 );
                                });

        $resource = new Collection($parks, new ParqueTransformer);
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function programmed()
    {
        $now = Carbon::now();
        $data = ProgrammingView::query()
                                ->select( 'park', 'park_name', 'park_code', 'upz_name', DB::raw('COUNT(park) as total_programmed') )
                                ->where('status', true)
                                ->whereBetween('execution_date', [ $now->startOfMonth()->format('Y-m-d'), $now->endOfMonth()->format('Y-m-d')  ])
                                ->groupBy('park')
                                ->having('total_programmed', '>', 1)
                                ->get();

        return response()->json( [
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
