<?php

namespace App\Http\Controllers\ProgrammingExecution;

use App\Acta;
use App\Commitments;
use App\Execution;
use App\ExecutionView;
use App\Images;
use App\Programacion;
use App\ProgrammingView;
use App\Transformers\Acta\ActaTransformer;
use App\Transformers\Commitments\CommitmentTransformer;
use App\Transformers\Execution\ExecutionViewTransformer;
use App\Transformers\Image\ImageTransformer;
use App\Transformers\Process\ProcessTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ProgrammingExecutionController extends Controller
{
    public function index(Programacion $programming)
    {
        $executions = ExecutionView::query()->where('programming_id', $programming->i_pk_id)->get();

        if ( $this->userValidation( $programming->i_pk_id ) ) {
            return response()->json([
                'message'  => trans('validation.handler.resource_not_found'),
                'code'  => 404
            ], 404);
        }

        $resource = new Collection($executions, new ExecutionViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        $data = $rootScope->toArray()['data'];

        if ( count( $data ) > 0 ) {
            $header = [
                'id'    =>  mb_convert_case( trans('validation.attributes.id'), MB_CASE_UPPER, 'UTF-8' ),
                'programming_id'    =>  mb_convert_case( trans('validation.attributes.programming_id'), MB_CASE_UPPER, 'UTF-8' ),
                'entity_id' =>  mb_convert_case( trans('validation.attributes.entity_id'), MB_CASE_UPPER, 'UTF-8' ),
                'entity_name'   =>  mb_convert_case( trans('validation.attributes.entity_name'), MB_CASE_UPPER, 'UTF-8' ),
                'type_id'   =>  mb_convert_case( trans('validation.attributes.type_id'), MB_CASE_UPPER, 'UTF-8' ),
                'type_population'   =>  mb_convert_case( trans('validation.attributes.type_population'), MB_CASE_UPPER, 'UTF-8' ),
                'condition_id'  =>  mb_convert_case( trans('validation.attributes.condition_id'), MB_CASE_UPPER, 'UTF-8' ),
                'condition_name'    =>  mb_convert_case( trans('validation.attributes.condition_name'), MB_CASE_UPPER, 'UTF-8' ),
                'situation_id'  =>  mb_convert_case( trans('validation.attributes.situation_id'), MB_CASE_UPPER, 'UTF-8' ),
                'situation_name'    =>  mb_convert_case( trans('validation.attributes.situation_name'), MB_CASE_UPPER, 'UTF-8' ),
                '0_5_f' =>  mb_convert_case( trans('validation.attributes.0_5_f'), MB_CASE_UPPER, 'UTF-8' ),
                '0_5_m' =>  mb_convert_case( trans('validation.attributes.0_5_m'), MB_CASE_UPPER, 'UTF-8' ),
                '6_12_f'    =>  mb_convert_case( trans('validation.attributes.6_12_f'), MB_CASE_UPPER, 'UTF-8' ),
                '6_12_m'    =>  mb_convert_case( trans('validation.attributes.6_12_m'), MB_CASE_UPPER, 'UTF-8' ),
                '13_17_f'   =>  mb_convert_case( trans('validation.attributes.13_17_f'), MB_CASE_UPPER, 'UTF-8' ),
                '13_17_m'   =>  mb_convert_case( trans('validation.attributes.13_17_m'), MB_CASE_UPPER, 'UTF-8' ),
                '18_26_f'   =>  mb_convert_case( trans('validation.attributes.18_26_f'), MB_CASE_UPPER, 'UTF-8' ),
                '18_26_m'   =>  mb_convert_case( trans('validation.attributes.18_26_m'), MB_CASE_UPPER, 'UTF-8' ),
                '27_59_f'   =>  mb_convert_case( trans('validation.attributes.27_59_f'), MB_CASE_UPPER, 'UTF-8' ),
                '27_59_m'   =>  mb_convert_case( trans('validation.attributes.27_59_m'), MB_CASE_UPPER, 'UTF-8' ),
                '60_f'  =>  mb_convert_case( trans('validation.attributes.60_f'), MB_CASE_UPPER, 'UTF-8' ),
                '60_m'  =>  mb_convert_case( trans('validation.attributes.60_m'), MB_CASE_UPPER, 'UTF-8' ),
                'subtotal'  =>  mb_convert_case( trans('validation.attributes.subtotal'), MB_CASE_UPPER, 'UTF-8' ),
                'created_at'    =>  mb_convert_case( trans('validation.attributes.created_at'), MB_CASE_UPPER, 'UTF-8' ),
            ];
            $sub = collect( $data );
            $subtotal = [
                'id'    =>  null,
                'programming_id'    => null,
                'entity_id' =>  null,
                'entity_name'   =>  null,
                'type_id'   =>  null,
                'type_population'   =>  null,
                'condition_id'  =>  null,
                'condition_name'    => null,
                'situation_id'  =>  null,
                'situation_name'    =>  "TOTAL",
                '0_5_f' =>  $sub->sum('0_5_f'),
                '0_5_m' =>  $sub->sum('0_5_m'),
                '6_12_f'    =>  $sub->sum('6_12_f'),
                '6_12_m'    =>  $sub->sum('6_12_m'),
                '13_17_f'   =>  $sub->sum('13_17_f'),
                '13_17_m'   =>  $sub->sum('13_17_m'),
                '18_26_f'   =>  $sub->sum('18_26_f'),
                '18_26_m'   =>  $sub->sum('18_26_m'),
                '27_59_f'   =>  $sub->sum('27_59_f'),
                '27_59_m'   =>  $sub->sum('27_59_m'),
                '60_f'  =>  $sub->sum('60_f'),
                '60_m'  =>  $sub->sum('60_m'),
                'subtotal'  =>  $sub->sum('subtotal'),
                'created_at'    =>  null,
            ];
            $data = array_prepend( $data, $header );
            $data = array_merge( $data, [ $subtotal ] );
        }

        return response()->json(['data' => $data], 200 );
    }

    public function file( Programacion $programming )
    {
        $executions = Acta::query()->where('i_fk_id_programacion', $programming->i_pk_id)->get();

        if ( $this->userValidation( $programming->i_pk_id ) ) {
            return response()->json([
                'message'  => trans('validation.handler.resource_not_found'),
                'code'  => 404
            ], 404);
        }

        if ( $executions ) {
            $resource = new Collection($executions, new ActaTransformer() );
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json($rootScope->toArray(), 200 );
        }
        return response()->json([ 'data' => '', 'code' => 200 ], 200 );
    }

    public function images( Programacion $programming )
    {
        $images = Images::query()->where('i_fk_id_programacion', $programming->i_pk_id)->get();
        if ( $this->userValidation( $programming->i_pk_id ) ) {
            return response()->json([
                'message'  => trans('validation.handler.resource_not_found'),
                'code'  => 404
            ], 404);
        }

        $resource = new Collection($images, new ImageTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200 );
    }

    public function commitments( Programacion $programming )
    {
        $commitments = Commitments::query()->where('i_fk_id_programacion', $programming->i_pk_id)->get();

        if ( $this->userValidation( $programming->i_pk_id ) ) {
            return response()->json([
                'message'  => trans('validation.handler.resource_not_found'),
                'code'  => 404
            ], 404);
        }

        $resource = new Collection($commitments, new CommitmentTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200 );
    }

    public function store(Programacion $programming, Request $request)
    {
        if ( (boolean) $programming->i_estado == true ) {
            if ( $request->hasFile('file') && $request->has('file_type') &&  $request->get('request') == 'agreement' ) {
                if ( $this->saveAgreement( $request, $programming ) ) {
                    return response()->json([
                        'data'  =>  trans('validation.handler.success'),
                        'code'  => 200
                    ], 200);
                }
            }

            if ( $request->get('request') == 'execution' ) {
                if ( $this->saveExecutions( $request, $programming ) ) {
                    return response()->json([
                        'data'  =>  trans('validation.handler.success'),
                        'code'  => 200
                    ], 200);
                }
            }

            if ( $request->get('request') == 'images' ) {
                $this->saveImages( $request, $programming );
                return response()->json([
                    'data'  =>  trans('validation.handler.success'),
                    'code'  => 200
                ], 200);
            }

            if ( $request->get('request') == 'commitment' ) {
                return $this->saveCommitments( $request, $programming );
            }
            return response()->json([
                'message'  =>  trans('validation.handler.unexpected_failure'),
                'code'  => 422
            ], 422);
        } else {
            return response()->json([
                'message'  =>  trans('validation.handler.canceled'),
                'code'  => 422
            ], 422);
        }
    }

    public function storeSolution(Commitments $commitment, Request $request)
    {
        $commitment->i_estado    = true;
        $commitment->tx_solucion = $request->get('solution_description');
        $commitment->dt_solucion = $request->get('solved_at');
        if ( $commitment->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }
        return response()->json([
            'message'  =>  trans('validation.handler.unexpected_failure'),
            'code'  => 422
        ], 422);
    }

    public function saveAgreement( Request $request, $programming )
    {
            //create a random name
            $name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();
            if ( $request->file('file')->move(public_path('storage/agreements'), $name) ) {
                $agreement = new Acta;
                $agreement->i_fk_id_programacion = $programming->i_pk_id;
                $agreement->i_fk_tipo = $request->get('file_type');
                $agreement->vc_path = $name;
                $agreement->vc_file_name = $request->file('file')->getClientOriginalName();
                return $agreement->save();
            }
        return false;
    }

    public function saveCommitments(Request $request, $programming)
    {
        $expired_at = Carbon::parse( $request->get('expired_at') )->format('Y-m-d');
        $schedules = ProgrammingView::query()->where('execution_date', $expired_at)
            ->where('status', true)
            ->where('user', $_SESSION['Id_funcionario'] )->get()->toArray();

        $start_init = Carbon::parse( "{$expired_at} {$request->get('initial_time')}");
        $start_end  = Carbon::parse( "{$expired_at} {$request->get('end_time')}");

        $created = array_where( $schedules, function ( $key, $value ) use ( $start_init, $start_end ) {
            $created_init = Carbon::parse( "{$value['execution_date']} {$value['initial_time']}");
            $created_end  = Carbon::parse( "{$value['execution_date']} {$value['end_time']}");
            return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
        });

        if ( count( $created ) > 0 ) {
            return response()->json([
                'message'  => trans('validation.handler.exist'),
                'code'  => 422
            ], 422);
        }

        $commitments = Commitments::query()->where('d_fecha', $expired_at)
            ->whereHas('programming_view', function ($query) {
                return $query->where('user', $_SESSION['Id_funcionario'] )->where('status', true);
            })->get()->toArray();

        $start_init = Carbon::parse( "{$expired_at} {$request->get('initial_time')}");
        $start_end  = Carbon::parse( "{$expired_at} {$request->get('end_time')}");

        $created = array_where( $commitments, function ( $key, $value ) use ( $start_init, $start_end ) {
            $created_init = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_ini']}");
            $created_end  = Carbon::parse( "{$value['d_fecha']} {$value['t_hora_fin']}");
            return ( $start_init->between( $created_init, $created_end ) || $start_end->between( $created_init, $created_end ) );
        });

        if ( count( $created ) > 0 ) {
            return response()->json([
                'message'  => trans('validation.handler.commit_exist'),
                'code'  => 422
            ], 422);
        }

        $commitment = new Commitments;
        $commitment->i_fk_id_programacion = $programming->i_pk_id;
        $commitment->vc_responsable       = $request->get('responsable');
        $commitment->tx_compromiso        = $request->get('commitment_description');
        $commitment->d_fecha              = $request->get('expired_at');
        $commitment->t_hora_ini           = $request->get('initial_time');
        $commitment->t_hora_fin           = $request->get('end_time');
        $commitment->save();

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  => 200
        ], 200);
    }

    public function saveExecutions(Request $request, $programming)
    {
        $execution = new Execution;
        $execution->i_fk_id_programacion    = $programming->i_pk_id;
        $execution->i_fk_id_entidad = $request->has('entity') ? $request->get('entity') : null;
        $execution->i_fk_id_tipo    = $request->has('type') ? $request->get('type') : null;
        $execution->i_fk_id_condicion   = $request->has('condition') ? $request->get('condition') : null;
        $execution->i_fk_id_situacion   = $request->has('situation') ? $request->get('situation') : null;
        $execution->i_0a5_f = $request->has('age_0_5_f') ?  $request->get('age_0_5_f')  : 0;
        $execution->i_0a5_m = $request->has('age_0_5_m') ?  $request->get('age_0_5_m')  : 0;
        $execution->i_6a12_f    = $request->has('age_6_12_f') ?  $request->get('age_6_12_f')  : 0;
        $execution->i_6a12_m    = $request->has('age_6_12_m') ?  $request->get('age_6_12_m')  : 0;
        $execution->i_13a17_f   = $request->has('age_13_17_f') ?  $request->get('age_13_17_f')  : 0;
        $execution->i_13a17_m   = $request->has('age_13_17_m') ?  $request->get('age_13_17_m')  : 0;
        $execution->i_18a26_f   = $request->has('age_18_26_f') ?  $request->get('age_18_26_f')  : 0;
        $execution->i_18a26_m   = $request->has('age_18_26_m') ?  $request->get('age_18_26_m')  : 0;
        $execution->i_27a59_f   = $request->has('age_27_59_f') ?  $request->get('age_27_59_f')  : 0;
        $execution->i_27a59_m   = $request->has('age_27_59_m') ?  $request->get('age_27_59_m')  : 0;
        $execution->i_60_f  = $request->has('age_60_f') ?  $request->get('age_60_f')  : 0;
        $execution->i_60_m  = $request->has('age_60_m') ?  $request->get('age_60_m')  : 0;
        return $execution->save();
    }

    public function saveImages( Request $request, $programming )
    {
        if ( $request->hasFile('image_1')  ) {
            $name_1 = substr_replace(sha1(microtime(true)), '', 20).'_1.'.$request->file('image_1')->getClientOriginalExtension();
            if ( $request->file('image_1')->move( public_path('storage/images'), $name_1  ) ) {
                $image_1 = new Images;
                $image_1->i_fk_id_programacion = $programming->i_pk_id;
                $image_1->tx_descripcion = $request->get('description_1');
                $image_1->bl_is_related =  $request->get('confirm_1') == "true" ? 1 : 0;
                $image_1->vc_file_name = $request->file('image_1')->getClientOriginalName();
                $image_1->vc_path = $name_1;
                return $image_1->save();
            }

        } else {
            if ( $request->has('description_1') ) {
                $image_1 = new Images;
                $image_1->i_fk_id_programacion = $programming->i_pk_id;
                $image_1->tx_descripcion = $request->get('description_1');
                $image_1->bl_is_related = $request->get('confirm_1') == "true" ? 1 : 0;
                return $image_1->save();
            } else {
                return false;
            }
        }
    }

    public function userValidation( $programming )
    {
        $program = ProgrammingView::query()->where( 'id', $programming )->where('user', $_SESSION['Id_funcionario'])->first();

        if ( !$program && (int) $_SESSION['Usuario']['Permisos']['admin'] == 0 ) {
            return true;
        }
        return false;
    }

    public function deleteFile( $id )
    {
        $file = Acta::query()->where( 'i_pk_id', $id )->first();

        if ( isset( $file->i_fk_id_programacion ) && $this->userValidation( $file->i_fk_id_programacion ) ) {
            if ( file_exists( public_path('storage/agreements').$file->vc_path ) ) {
                unlink( public_path('storage/agreements').$file->vc_path );
            }
            $file->delete();

            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }
        return response()->json([
            'message'  => trans('validation.handler.resource_not_found'),
            'code'  => 404
        ], 404);
    }
}
