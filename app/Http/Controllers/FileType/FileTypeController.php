<?php

namespace App\Http\Controllers\FileType;


use App\FileType;
use App\Http\Controllers\Controller;
use App\Transformers\FileType\FilleTypeTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class FileTypeController extends Controller
{
    public function index()
    {
        $data = FileType::all();
        $resource = new Collection($data, new FilleTypeTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
