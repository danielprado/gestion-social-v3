<?php


namespace App\Http\Controllers\Report;


use App\Http\Controllers\Controller;
use App\ProgrammingView;
use App\Transformers\Programming\ProgrammingTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ReportController extends Controller
{
    public function index( Request $request )
    {
        $initial_date = $request->has('start_day') ? $request->get('start_day') : Carbon::now()->startOfMonth()->format('Y-m-d');
        $final_date   = $request->has('end_day')   ? $request->get('end_day') : Carbon::now()->endOfMonth()->format('Y-m-d');
        $data = ProgrammingView::query();
        $data = $request->has('professional') ? $data->where('user', (int) $request->get('professional') ) : $data;
        $data = $data->whereBetween('execution_date', [$initial_date, $final_date])->get();

        $data = $data->map( function ( $item ) {
            return ( new ProgrammingTransformer() )->transformToExcel( $item );
        })->toArray();


        Excel::load( public_path('excel/REPORTE_GENERAL.xlsx'), function ($file) use ( $data, $initial_date, $final_date ) {

            $file->sheet(0, function ($sheet) use ( $data, $initial_date, $final_date ) {

                $row = 8;

                $sheet->cell("C3", function($cell)  {
                    $cell->setValue( isset(  $_SESSION['auth']->full_name ) ?  $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("I3", function($cell) use ($initial_date, $final_date)  {
                    $cell->setValue( "DEL  $initial_date AL $final_date " );
                });
                $sheet->cell("C4", function($cell)  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });


                foreach ( $data as $datum ) {
                    $sheet->row($row, $datum);
                    $row++;
                }


                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A8:BA$sum")->applyFromArray($styleArray);
            });

        })->export('xlsx');
    }
}