<?php

namespace App\Http\Controllers\Attemption;

use App\Attemption;
use App\Transformers\Attemption\AttemptionTransformer;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class AttemptionController extends Controller
{
    public function index()
    {
        $data = Attemption::all();
        $resource = new Collection($data, new AttemptionTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
