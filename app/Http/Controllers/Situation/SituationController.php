<?php

namespace App\Http\Controllers\Situation;

use App\Http\Controllers\Controller;
use App\Situation;
use App\Transformers\Situation\SituationTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SituationController extends Controller
{
    public function index()
    {
        $data = Situation::all();
        $resource = new Collection($data, new SituationTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
