<?php

namespace App\Http\Controllers\Professionals;

use App\ActivityAccess;
use App\Transformers\Professionals\ProfessionalTransformer;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProfessionalController extends Controller
{
    public function index()
    {
        $activities =  DB::connection( config('usuarios.conexion') )->table('actividades')->select('Id_Actividad')->where('Id_Modulo', env('MODULE_ID'))->get();
        $users = ActivityAccess::query()->whereIn('Id_Actividad', json_decode(json_encode($activities), true))->get(['Id_Persona'])->pluck('Id_Persona')->toArray();
        $professionals = User::query()->whereIn('Id_Persona', $users)->get();

        $resource = new Collection($professionals, new ProfessionalTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
