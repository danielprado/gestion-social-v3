<?php

namespace App\Http\Controllers\Type;

use App\Tipo;
use App\Transformers\Tipo\TypeTransformer;

use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class TypeController extends Controller
{
    public function index()
    {
        $data = Tipo::all();
        $resource = new Collection($data, new TypeTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }
}
