<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acta extends Model
{
    protected $table = 'archivoacta';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['i_fk_id_programacion','i_fk_tipo','vc_path', 'vc_file_name'];
}
