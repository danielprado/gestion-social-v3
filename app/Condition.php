<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'condiciones';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_condicion','i_estado'];
}
