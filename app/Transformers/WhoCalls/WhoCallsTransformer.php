<?php


namespace App\Transformers\WhoCalls;


use App\QuienConvoca;
use League\Fractal\TransformerAbstract;

class WhoCallsTransformer extends TransformerAbstract
{
    public function transform( QuienConvoca $whoCalls )
    {
        return [
            'id'        =>  isset( $whoCalls->i_pk_id )   ? $whoCalls->i_pk_id : 0,
            'name'      =>  isset( $whoCalls->vc_nombre ) ? $whoCalls->vc_nombre : null,
            'has_input' =>  ( $whoCalls->has_input ) ? true : false
        ];
    }
}