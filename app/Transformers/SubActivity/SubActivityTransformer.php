<?php


namespace App\Transformers\SubActivity;


use App\Activity;
use App\SubActivity;
use League\Fractal\TransformerAbstract;

class SubActivityTransformer extends TransformerAbstract
{
    public function transform(SubActivity $activity)
    {
        return [
            'id'    =>  isset( $activity->id )   ? (int) $activity->id : null,
            'name'  =>  isset( $activity->sub_activity ) ? $activity->sub_activity : null,
        ];
    }
}