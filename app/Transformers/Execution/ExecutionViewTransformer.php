<?php


namespace App\Transformers\Execution;


use App\ExecutionView;
use League\Fractal\TransformerAbstract;

class ExecutionViewTransformer extends TransformerAbstract
{
    public function transform( ExecutionView $execution )
    {
        return [
            'id'    =>  isset( $execution->id ) ? $execution->id : 0,
            'programming_id'    =>  isset( $execution->programming_id ) ? $execution->programming_id : 0,
            'entity_id' =>  isset( $execution->entity_id ) ? $execution->entity_id : 0,
            'entity_name'   =>  isset( $execution->entity_name ) ? $execution->entity_name : null,
            'type_id'   =>  isset( $execution->type_id ) ? $execution->type_id : 0,
            'type_population'   =>  isset( $execution->type_population ) ? $execution->type_population : null,
            'condition_id'  =>  isset( $execution->condition_id ) ? $execution->condition_id : 0,
            'condition_name'    =>  isset( $execution->condition_name ) ? $execution->condition_name : null,
            'situation_id'  =>  isset( $execution->situation_id ) ? $execution->situation_id : 0,
            'situation_name'    =>  isset( $execution->situation_name ) ? $execution->situation_name : null,
            '0_5_f'     =>  isset( $execution->{ '0_5_f' } ) ? (int) $execution->{ '0_5_f' } : 0,
            '0_5_m'     =>  isset( $execution->{ '0_5_m' } ) ? (int) $execution->{ '0_5_m' } : 0,
            '6_12_f'    =>  isset( $execution->{ '6_12_f' } ) ? (int) $execution->{ '6_12_f' } : 0,
            '6_12_m'    =>  isset( $execution->{ '6_12_m' } ) ? (int) $execution->{ '6_12_m' } : 0,
            '13_17_f'   =>  isset( $execution->{ '13_17_f' } ) ? (int) $execution->{ '13_17_f' } : 0,
            '13_17_m'   =>  isset( $execution->{ '13_17_m' } ) ? (int) $execution->{ '13_17_m' } : 0,
            '18_26_f'   =>  isset( $execution->{ '18_26_f' } ) ? (int) $execution->{ '18_26_f' } : 0,
            '18_26_m'   =>  isset( $execution->{ '18_26_m' } ) ? (int) $execution->{ '18_26_m' } : 0,
            '27_59_f'   =>  isset( $execution->{ '27_59_f' } ) ? (int) $execution->{ '27_59_f' } : 0,
            '27_59_m'   =>  isset( $execution->{ '27_59_m' } ) ? (int) $execution->{ '27_59_m' } : 0,
            '60_f'      =>  isset( $execution->{ '60_f' } ) ? (int) $execution->{ '60_f' } : 0,
            '60_m'      =>  isset( $execution->{ '60_m' } ) ? (int) $execution->{ '60_m' } : 0,
            'subtotal'  =>  (int) $this->sum( $execution ),
            'created_at'    =>  isset( $execution->created_at ) ? $execution->created_at : null,
        ];
    }

    public function sum( $execution )
    {
        $sum_0_5_f = isset( $execution->{ '0_5_f' } ) ? (int) $execution->{ '0_5_f' } : 0;
        $sum_0_5_m = isset( $execution->{ '0_5_m' } ) ? (int) $execution->{ '0_5_m' } : 0;
        $sum_6_12_f = isset( $execution->{ '6_12_f' } ) ? (int) $execution->{ '6_12_f' } : 0;
        $sum_6_12_m = isset( $execution->{ '6_12_m' } ) ? (int) $execution->{ '6_12_m' } : 0;
        $sum_13_17_f = isset( $execution->{ '13_17_f' } ) ? (int) $execution->{ '13_17_f' } : 0;
        $sum_13_17_m = isset( $execution->{ '13_17_m' } ) ? (int) $execution->{ '13_17_m' } : 0;
        $sum_18_26_f = isset( $execution->{ '18_26_f' } ) ? (int) $execution->{ '18_26_f' } : 0;
        $sum_18_26_m = isset( $execution->{ '18_26_m' } ) ? (int) $execution->{ '18_26_m' } : 0;
        $sum_27_59_f = isset( $execution->{ '27_59_f' } ) ? (int) $execution->{ '27_59_f' } : 0;
        $sum_27_59_m = isset( $execution->{ '27_59_m' } ) ? (int) $execution->{ '27_59_m' } : 0;
        $sum_60_f = isset( $execution->{ '60_f' } ) ? (int) $execution->{ '60_f' } : 0;
        $sum_60_m = isset( $execution->{ '60_m' } ) ? (int) $execution->{ '60_m' } : 0;
        return $sum_0_5_f + $sum_0_5_m + $sum_6_12_f + $sum_6_12_m + $sum_13_17_f + $sum_13_17_m + $sum_18_26_f + $sum_18_26_m + $sum_27_59_f + $sum_27_59_m + $sum_60_f + $sum_60_m;
    }
}