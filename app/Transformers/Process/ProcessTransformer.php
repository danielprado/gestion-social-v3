<?php


namespace App\Transformers\Process;


use App\Process;
use League\Fractal\TransformerAbstract;

class ProcessTransformer extends TransformerAbstract
{
    public function transform(Process $process)
    {
        return [
            'id'    =>  isset( $process->i_pk_id )   ? (int) $process->i_pk_id : null,
            'name'  =>  isset( $process->vc_nombre ) ? $process->vc_nombre : null,
        ];
    }
}