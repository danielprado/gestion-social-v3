<?php


namespace App\Transformers\Activity;


use App\Activity;
use League\Fractal\TransformerAbstract;

class ActivityTransformer extends TransformerAbstract
{
    public function transform(Activity $activity)
    {
        return [
            'id'    =>  isset( $activity->i_pk_id )   ? (int) $activity->i_pk_id : null,
            'name'  =>  isset( $activity->vc_nombre ) ? $activity->vc_nombre : null,
            'has_select'    =>  ( $activity->has_select ) ? true : false
        ];
    }
}