<?php


namespace App\Transformers\Programming;


use App\Commitments;
use App\CommitmentView;
use App\ExecutionView;
use App\FileView;
use App\Images;
use App\Programacion;
use App\ProgrammingView;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class ProgrammingTransformer extends TransformerAbstract
{
    public function transform( ProgrammingView $programming )
    {

        $is_tracing = DB::table('agreements_2018')->where( 'park_code', isset( $programming->park_code ) ? $programming->park_code : '' )->count();

        return [
            'id'                =>  isset( $programming->id ) ? (int) $programming->id : null,
            'user'              =>  isset( $programming->user ) ? (int) $programming->user : null,
            'can_edit'          =>  isset( $programming->user ) ? (int) $programming->user == (int) $_SESSION['Id_funcionario'] : false,
            'full_name'         =>  isset( $programming->full_name ) ? $programming->full_name : null,
            'execution_date'    =>  isset( $programming->execution_date ) ? $programming->execution_date : null,
            'initial_time'      =>  isset( $programming->initial_time ) ? $programming->initial_time : null,
            'end_time'          =>  isset( $programming->end_time ) ? $programming->end_time : null,
            'park'              =>  isset( $programming->park ) ? (int) $programming->park : null,
            'park_name'         =>  isset( $programming->park_name ) ? $programming->park_name : null,
            'park_address'      =>  isset( $programming->park_address ) ? $programming->park_address : null,
            'park_code'         =>  isset( $programming->park_code ) ? $programming->park_code : null,
            'place'             =>  isset( $programming->place ) ? $programming->place : null,
            'upz'               =>  isset( $programming->upz ) ? (int) $programming->upz : null,
            'upz_name'          =>  isset( $programming->upz_name ) ? $programming->upz_name : null,
            'objective'         =>  isset( $programming->objective ) ? $programming->objective : null,
            'type_of_meeting'   =>  isset( $programming->reunion_type ) ? (int) $programming->reunion_type : null,
            'type_of_meeting_name'  =>  isset( $programming->reunion_type_name ) ? $programming->reunion_type_name : null,
            'attention'         =>  isset( $programming->attention ) ? (int) $programming->attention : null,
            'attention_name'    =>  isset( $programming->attention_name ) ? $programming->attention_name : null,
            'who_calls'         =>  isset( $programming->who_calls ) ? (int) $programming->who_calls : null,
            'who_calls_name'    =>  isset( $programming->who_calls_name ) ? $programming->who_calls_name : null,
            'who_calls_text'    =>  isset( $programming->who_calls_text ) ? $programming->who_calls_text : null,
            'request'           =>  isset( $programming->request ) ? (int) $programming->request : null,
            'request_name'      =>  isset( $programming->request_name ) ? $programming->request_name : null,
            'process'           =>  isset( $programming->process ) ? (int) $programming->process : null,
            'process_name'      =>  isset( $programming->process_name ) ? $programming->process_name : null,
            'activity'          =>  isset( $programming->activity ) ? (int) $programming->activity : null,
            'activity_name'     =>  isset( $programming->activity_name ) ? $programming->activity_name : null,
            'subactivity'       =>  isset( $programming->subactivity ) ? (int) $programming->subactivity : null,
            'subactivity_name'  =>  isset( $programming->subactivity_name ) ? $programming->subactivity_name : null,
            'status'            =>  isset( $programming->status ) ? (boolean) $programming->status : null,
            'has_execution'     =>  isset( $programming->id ) ? (int) ExecutionView::query()->where('programming_id', (int) $programming->id )->count() : 0,
            'has_commitment'    =>  isset( $programming->id ) ? (int) Commitments::query()->where('i_fk_id_programacion', (int) $programming->id )->count() : 0,
            'is_tracing'        =>  (boolean) ($is_tracing > 0),
            'who_cancel'        =>  isset( $programming->who_cancel ) ? $programming->who_cancel : null,
            'text_cancel'       =>  isset( $programming->text_cancel ) ? $programming->text_cancel : null,
            'created_at'        =>  isset( $programming->created_at ) ? $programming->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset( $programming->updated_at ) ? $programming->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }


    public function transformToExcel( $programming )
    {
        $is_tracing = DB::table('agreements_2018')->where( 'park_code', isset( $programming->park_code ) ? $programming->park_code : '' )->count();
        $status = isset( $programming->status ) ? (boolean) $programming->status : false;

        $program = [
            'id'                =>  isset( $programming->id ) ? (int) $programming->id : null,
            'execution_date'    =>  isset( $programming->execution_date ) ? $programming->execution_date : null,
            'initial_time'      =>  isset( $programming->initial_time ) ? $programming->initial_time : null,
            'end_time'          =>  isset( $programming->end_time ) ? $programming->end_time : null,
            'type_of_meeting_name'  =>  isset( $programming->reunion_type_name ) ? $programming->reunion_type_name : null,
            'attention_name'    =>  isset( $programming->attention_name ) ? $programming->attention_name : null,
            'full_name'         =>  isset( $programming->full_name ) ? $this->toUpper( $programming->full_name ) : null,
            'park_code'         =>  isset( $programming->park_code ) ? $this->toUpper( $programming->park_code ) : null,
            'park_name'         =>  isset( $programming->park_name ) ? $this->toUpper( $programming->park_name ) : null,
            'park_address'      =>  isset( $programming->park_address ) ? $this->toUpper( $programming->park_address ) : null,
            'upz_name'          =>  isset( $programming->upz_name ) ? $this->toUpper( $programming->upz_name ) : null,
            'process_name'      =>  isset( $programming->process_name ) ? $this->toUpper( $programming->process_name ) : null,
            'activity_name'     =>  isset( $programming->activity_name ) ? $this->toUpper( $programming->activity_name ) : null,
            'subactivity_name'  =>  isset( $programming->subactivity_name ) ? $this->toUpper( $programming->subactivity_name ) : null,
            'who_calls_name'    =>  isset( $programming->who_calls_name ) ? $this->toUpper( $programming->who_calls_name ) : null,
            'who_calls_text'    =>  isset( $programming->who_calls_text ) ? $this->toUpper( $programming->who_calls_text ) : null,
            'request_name'      =>  isset( $programming->request_name ) ? $this->toUpper( $programming->request_name ) : null,
            'objective'         =>  isset( $programming->objective ) ? $this->toUpper( $programming->objective ) : null,
            'status'            =>  $status ? 'ACTIVO' : 'CANCELADO',
            'who_cancel'        =>  isset( $programming->who_cancel ) ? $this->toUpper( $programming->who_cancel ) : null,
            'text_cancel'       =>  isset( $programming->text_cancel ) ? $this->toUpper( $programming->text_cancel ) : null,
            'is_tracing'        =>  ($is_tracing > 0) ? 'SEGUIMIENTO' : 'NUEVA VISITA',
            'created_at'        =>  isset( $programming->created_at ) ? $programming->created_at : null,
        ];

        $programming_id = isset( $programming->id ) ? (int) $programming->id : null;

        $files  = $this->getFiles( $programming_id );
        $images = $this->getImages( $programming_id );
        $commits = $this->getCommitments(  $programming_id );
        $execution = $this->getExecution(  $programming_id );

        $array = array_merge( $program, $files, $images, $commits, $execution );

        return $array;
    }

    public function getFiles( $id = null )
    {
        $types = '';
        $names = '';
        $created_at = '';
        if ( isset( $id ) ) {
            $files = FileView::query()->where('programming_id', $id)->get();
            foreach ( $files as $file ) {
                $file_id   = isset( $file->id ) ? $file->id : null;
                $file_type = isset( $file->file_type ) ? $file->file_type : null;
                $file_name = isset( $file->path ) ? asset( 'public/storage/agreements/'.$file->path ) : null;
                $file_date = isset( $file->created_at ) ? $file->created_at : null;

                $types.= "({$file_id}) - {$file_type}\n";
                $names.= "({$file_id}) - {$file_name}\n";
                $created_at.= "({$file_id}) - {$file_date}\n";
            }

        }

        return [
            'file_types' =>  $types,
            'file_names' =>  $names,
            'file_dates' =>  $created_at,
        ];
    }

    public function getImages( $id = null )
    {
        $links = '';
        $description = '';
        $created_at = '';
        if ( isset( $id ) ) {
            $images = Images::query()->where('i_fk_id_programacion', $id)->get();
            foreach ( $images as $file ) {
                $file_id   = isset( $file->i_pk_id ) ? $file->i_pk_id : null;
                $file_type = isset( $file->tx_descripcion ) ? $this->toUpper( $file->tx_descripcion ) : null;
                $file_name = isset( $file->vc_path ) ? asset( 'public/storage/images/'.$file->vc_path ) : null;
                $file_date = isset( $file->created_at ) ? $file->created_at : null;

                $links.= "({$file_id}) - {$file_name}\n";
                $description.= "({$file_id}) - {$file_type}\n";
                $created_at.= "({$file_id}) - {$file_date}\n";
            }
        }
        return [
            'image_links' =>  $links,
            'image_descriptions' =>  $description,
            'image_dates' =>  $created_at,
        ];
    }

    public function getCommitments( $id = null )
    {
        $responsables = '';
        $commitments = '';
        $commit_dates = '';
        $follow = '';
        $follow_date = '';
        $created_at = '';
        if ( isset( $id ) ) {
            $commits = CommitmentView::query()->where('programming_id', $id)->get();
            foreach ( $commits as $data ) {
                $commit_id = isset( $data->id ) ? $data->id : null;
                $responsable = isset( $data->responsable ) ? $this->toUpper( $data->responsable ) : null;
                $commit = isset( $data->commitment ) ? $this->toUpper( $data->commitment ) : null;
                $date = isset( $data->date ) ? $data->date : null;
                $solution = isset( $data->solution ) ? $this->toUpper( $data->solution ) : null;
                $solved = isset( $data->solved_at ) ? $data->solved_at : null;
                $creation = isset( $data->created_at ) ? $data->created_at : null;

                $responsables.="({$commit_id}) - {$responsable}\n";
                $commitments.="({$commit_id}) - {$commit}\n";
                $commit_dates.="({$commit_id}) - {$date}\n";
                $follow.="({$commit_id}) - {$solution}\n";
                $follow_date.="({$commit_id}) - {$solved}\n";
                $created_at.="({$commit_id}) - {$creation}\n";
            }
        }
        return [
            'responsable'   => $responsables,
            'commits'   => $commitments,
            'commit_dates'  => $commit_dates,
            'follow'    => $follow,
            'follow_date'   => $follow_date,
            'commit_created_at'    => $created_at,
        ];
    }

    public function getExecution( $id = null )
    {
        $entities = '';
        $populations = '';
        $conditions = '';
        $situations = '';
        $f_0_5 = '';
        $m_0_5 = '';
        $f_6_12 = '';
        $m_6_12 = '';
        $f_13_17 = '';
        $m_13_17 = '';
        $f_18_26 = '';
        $m_18_26 = '';
        $f_27_59 = '';
        $m_27_59 = '';
        $f_60 = '';
        $m_60 = '';
        $subtotal = '';
        $total = 0;
        if ( isset( $id ) ) {
            $executions = ExecutionView::query()->where('programming_id', $id)->get();
            foreach ( $executions as $data ) {
                $execution_id = isset( $data->id ) ? $data->id : null;
                $entity = isset( $data->entity_name ) ? $data->entity_name : null;
                $population = isset( $data->type_population ) ? $data->type_population : null;
                $condition = isset( $data->condition_name ) ? $data->condition_name : null;
                $situation = isset( $data->situation_name ) ? $data->situation_name : null;
                $f_0_5_sum = isset( $data->{ '0_5_f' } ) ? (int) $data->{ '0_5_f' } : 0;
                $m_0_5_sum = isset( $data->{ '0_5_m' } ) ? (int) $data->{ '0_5_m' } : 0;
                $f_6_12_sum = isset( $data->{ '6_12_f' } ) ? (int) $data->{ '6_12_f' } : 0;
                $m_6_12_sum = isset( $data->{ '6_12_m' } ) ? (int) $data->{ '6_12_m' } : 0;
                $f_13_17_sum = isset( $data->{ '13_17_f' } ) ? (int) $data->{ '13_17_f' } : 0;
                $m_13_17_sum = isset( $data->{ '13_17_m' } ) ? (int) $data->{ '13_17_m' } : 0;
                $f_18_26_sum = isset( $data->{ '18_26_f' } ) ? (int) $data->{ '18_26_f' } : 0;
                $m_18_26_sum = isset( $data->{ '18_26_m' } ) ? (int) $data->{ '18_26_m' } : 0;
                $f_27_59_sum = isset( $data->{ '27_59_f' } ) ? (int) $data->{ '27_59_f' } : 0;
                $m_27_59_sum = isset( $data->{ '27_59_m' } ) ? (int) $data->{ '27_59_m' } : 0;
                $f_60_sum = isset( $data->{ '60_f' } ) ? (int) $data->{ '60_f' } : 0;
                $m_60_sum = isset( $data->{ '60_m' } ) ? (int) $data->{ '60_m' } : 0;
                $sub = $this->sum( $data );
                $total = $total + $sub;

                $entities.= "({$execution_id}) - {$entity}\n";
                $populations.= "({$execution_id}) - {$population}\n";
                $conditions.= "({$execution_id}) - {$condition}\n";
                $situations.= "({$execution_id}) - {$situation}\n";
                $f_0_5.= "({$execution_id}) - CANT: ( {$f_0_5_sum} )\n";
                $m_0_5.= "({$execution_id}) - CANT: ( {$m_0_5_sum} )\n";
                $f_6_12.= "({$execution_id}) - CANT: ( {$f_6_12_sum} )\n";
                $m_6_12.= "({$execution_id}) - CANT: ( {$m_6_12_sum} )\n";
                $f_13_17.= "({$execution_id}) - CANT: ( {$f_13_17_sum} )\n";
                $m_13_17.= "({$execution_id}) - CANT: ( {$m_13_17_sum} )\n";
                $f_18_26.= "({$execution_id}) - CANT: ( {$f_18_26_sum} )\n";
                $m_18_26.= "({$execution_id}) - CANT: ( {$m_18_26_sum} )\n";
                $f_27_59.= "({$execution_id}) - CANT: ( {$f_27_59_sum} )\n";
                $m_27_59.= "({$execution_id}) - CANT: ( {$m_27_59_sum} )\n";
                $f_60.= "({$execution_id}) - CANT: ( {$f_60_sum} )\n";
                $m_60.= "({$execution_id}) - CANT: ( {$m_60_sum} )\n";
                $subtotal.= "({$execution_id}) - CANT: ( {$sub} )\n";
            }
        }
        return [
            'entities'  => $entities,
            'populations'   => $populations,
            'conditions'    => $conditions,
            'situations'    => $situations,
            'f_0_5' => $f_0_5,
            'm_0_5' => $m_0_5,
            'f_6_12'    => $f_6_12,
            'm_6_12'    => $m_6_12,
            'f_13_17'   => $f_13_17,
            'm_13_17'   => $m_13_17,
            'f_18_26'   => $f_18_26,
            'm_18_26'   => $m_18_26,
            'f_27_59'   => $f_27_59,
            'm_27_59'   => $m_27_59,
            'f_60'  => $f_60,
            'm_60'  => $m_60,
            'subtotal'  => $subtotal,
            'total' => $total
        ];
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }

    public function sum( $execution )
    {
        $sum_0_5_f = isset( $execution->{ '0_5_f' } ) ? (int) $execution->{ '0_5_f' } : 0;
        $sum_0_5_m = isset( $execution->{ '0_5_m' } ) ? (int) $execution->{ '0_5_m' } : 0;
        $sum_6_12_f = isset( $execution->{ '6_12_f' } ) ? (int) $execution->{ '6_12_f' } : 0;
        $sum_6_12_m = isset( $execution->{ '6_12_m' } ) ? (int) $execution->{ '6_12_m' } : 0;
        $sum_13_17_f = isset( $execution->{ '13_17_f' } ) ? (int) $execution->{ '13_17_f' } : 0;
        $sum_13_17_m = isset( $execution->{ '13_17_m' } ) ? (int) $execution->{ '13_17_m' } : 0;
        $sum_18_26_f = isset( $execution->{ '18_26_f' } ) ? (int) $execution->{ '18_26_f' } : 0;
        $sum_18_26_m = isset( $execution->{ '18_26_m' } ) ? (int) $execution->{ '18_26_m' } : 0;
        $sum_27_59_f = isset( $execution->{ '27_59_f' } ) ? (int) $execution->{ '27_59_f' } : 0;
        $sum_27_59_m = isset( $execution->{ '27_59_m' } ) ? (int) $execution->{ '27_59_m' } : 0;
        $sum_60_f = isset( $execution->{ '60_f' } ) ? (int) $execution->{ '60_f' } : 0;
        $sum_60_m = isset( $execution->{ '60_m' } ) ? (int) $execution->{ '60_m' } : 0;
        return $sum_0_5_f + $sum_0_5_m + $sum_6_12_f + $sum_6_12_m + $sum_13_17_f + $sum_13_17_m + $sum_18_26_f + $sum_18_26_m + $sum_27_59_f + $sum_27_59_m + $sum_60_f + $sum_60_m;
    }
}