<?php


namespace App\Transformers\Programming;


use App\ProgrammingView;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProgrammingCalendarTransformer extends TransformerAbstract
{
    public function transform( ProgrammingView $view )
    {
        return [
            'id'    => (isset( $view->id )) ? $view->id : 0,
            'activity' => $view->activity_name,
            'title' => "{$view->park_code} - {$view->park_name}",
            'start' => $view->execution_date.' '.$view->initial_time,
            'end'   => $view->execution_date.' '.$view->end_time,
            'allDay'=> false,
            'className' =>  $this->getClassName( $view->execution_date )
        ];
    }

    public function getClassName( $date = null )
    {
        if ( isset( $date ) ) {
            $date = Carbon::parse( $date );

            if ( Carbon::now()->greaterThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-azure';
            }

            if ( Carbon::now()->lessThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-green';
            }

            if ( Carbon::now()->format('Y-m-d') == $date->format('Y-m-d') ) {
                return 'event-orange';
            }
        }

        return 'event-red';
    }
}