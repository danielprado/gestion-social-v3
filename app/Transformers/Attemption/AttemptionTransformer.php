<?php


namespace App\Transformers\Attemption;


use App\Attemption;
use League\Fractal\TransformerAbstract;

class AttemptionTransformer extends TransformerAbstract
{
    public function transform( Attemption $attemption )
    {
        return [
            'id'        => isset( $attemption->id ) ? $attemption->id : null,
            'journey'   => isset( $attemption->journey ) ? $attemption->journey : null,
            'start_hour'=> isset( $attemption->start_hour ) ? $attemption->start_hour : null,
            'end_hour'  => isset( $attemption->end_hour ) ? $attemption->end_hour : null,
        ];
    }
}