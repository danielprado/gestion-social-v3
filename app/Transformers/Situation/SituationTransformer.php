<?php


namespace App\Transformers\Situation;


use App\Situation;
use League\Fractal\TransformerAbstract;

class SituationTransformer extends TransformerAbstract
{
    public function transform(Situation $situation)
    {
        return [
            'id'    => isset( $situation->i_pk_id ) ? $situation->i_pk_id : null,
            'name'  => isset( $situation->vc_situacion ) ? $situation->vc_situacion : null,
        ];
    }
}