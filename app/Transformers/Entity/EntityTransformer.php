<?php


namespace App\Transformers\Entity;


use App\Entity;
use League\Fractal\TransformerAbstract;

class EntityTransformer extends TransformerAbstract
{
    public function transform( Entity $entity ) {
        return [
            'id'      =>  isset( $entity->idEntidad ) ? (int) $entity->idEntidad : null,
            'name'    =>  isset( $entity->Entidad )   ? $entity->Entidad : null,
        ];
    }
}