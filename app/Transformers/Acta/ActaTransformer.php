<?php


namespace App\Transformers\Acta;


use App\Acta;
use App\FileType;
use League\Fractal\TransformerAbstract;

class ActaTransformer extends TransformerAbstract
{
    public function transform(Acta $acta)
    {
        return [
            'id'   =>  isset( $acta->i_pk_id ) ? $acta->i_pk_id : 0,
            'type'   =>  isset( $acta->i_fk_tipo ) ?  FileType::where('id', $acta->i_fk_tipo)->first()->file_type : null,
            'description'    =>  isset( $acta->vc_file_name ) ? $acta->vc_file_name: null,
            'programming_id'  =>  isset( $acta->i_fk_id_programacion ) ? $acta->i_fk_id_programacion : 0,
            'path'    =>  isset( $acta->vc_path ) ? asset( 'public/storage/agreements/'.$acta->vc_path ) : null,
            'created_at'  =>  isset( $acta->created_at ) ? $acta->created_at->format('Y-m-d H:i:s') : 0,
        ];
    }
}