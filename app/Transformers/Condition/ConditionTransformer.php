<?php


namespace App\Transformers\Condition;


use App\Condition;
use League\Fractal\TransformerAbstract;

class ConditionTransformer extends TransformerAbstract
{
    public function transform(Condition $condition)
    {
        return [
            'id'    =>  isset( $condition->i_pk_id ) ? $condition->i_pk_id : null,
            'name'  =>  isset( $condition->vc_condicion ) ? $condition->vc_condicion : null,
        ];
    }
}