<?php


namespace App\Transformers\ReunionType;


use App\ReunionType;
use League\Fractal\TransformerAbstract;

class ReunionTypeTransformer extends TransformerAbstract
{
    public function transform(ReunionType $reunionType)
    {
        return [
            'id'            =>  isset( $reunionType->id )           ? $reunionType->id : 0,
            'reunion_type'  =>  isset( $reunionType->reunion_type ) ? $reunionType->reunion_type : null,
        ];
    }
}