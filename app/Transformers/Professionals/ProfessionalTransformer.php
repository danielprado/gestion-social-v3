<?php


namespace App\Transformers\Professionals;


use App\User;
use League\Fractal\TransformerAbstract;

class ProfessionalTransformer extends TransformerAbstract
{
    public function transform( User $user )
    {
        return [
            'id'            =>  isset( $user->Id_Persona) ? $user->Id_Persona : 0,
            'name'          =>  isset( $user->full_name ) ? $user->full_name  : '',
            'short_name'    =>  isset( $user->part_name ) ? $user->part_name  : '',
            'document'      =>  isset( $user->Cedula ) ? $user->Cedula  : '',
        ];
    }
}