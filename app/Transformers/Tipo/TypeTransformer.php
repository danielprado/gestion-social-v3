<?php


namespace App\Transformers\Tipo;


use App\Tipo;
use League\Fractal\TransformerAbstract;

class TypeTransformer extends TransformerAbstract
{
    public function transform(Tipo $type)
    {
        return [
            'id'    =>  isset( $type->i_pk_id ) ? $type->i_pk_id : null,
            'name'  =>  isset( $type->vc_tipo ) ? $type->vc_tipo : null,
        ];
    }
}