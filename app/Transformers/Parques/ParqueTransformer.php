<?php


namespace App\Transformers\Parques;



use App\Parque;
use Idrd\Parques\Repo\Upz;
use League\Fractal\TransformerAbstract;

class ParqueTransformer extends TransformerAbstract
{
    public function transform( Parque $parque )
    {
        return [
            'id'        =>  isset( $parque->Id )      ? (int) $parque->Id : 0,
            'code'      =>  isset( $parque->Id_IDRD ) ? $parque->Id_IDRD : null,
            'name'      =>  isset( $parque->Nombre )  ? ( isset($parque->Direccion) ) ? $parque->Id_IDRD.' '.$parque->Nombre.' '.$this->toUpper( $parque->Direccion ) : $parque->Nombre  : '',
            'upz'       =>  isset( $parque->Upz ) ? $parque->Upz : null,
            'upz_name'  =>  isset( $parque->Upz ) ? $this->toUpper( $this->getUpz( $parque->Upz ) ) : null,
        ];
    }

    public function getUpz( $upz = null )
    {
        if ( isset( $upz ) ) {
            $data = Upz::query()->where('cod_upz', $upz)->first();
            return isset( $data->Upz ) ? $data->Upz : '';
        } else {
            return '';
        }
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}