<?php


namespace App\Transformers\Commitments;


use App\Commitments;
use League\Fractal\TransformerAbstract;

class CommitmentTransformer extends TransformerAbstract
{
    public function transform( Commitments $commitment )
    {
        return [
            'id'                      => isset( $commitment->i_pk_id ) ? $commitment->i_pk_id : 0,
            'programming'             => isset( $commitment->i_fk_id_programacion ) ? $commitment->i_fk_id_programacion : null,
            'responsable'             => isset( $commitment->vc_responsable ) ? $commitment->vc_responsable : null,
            'commitment'              => isset( $commitment->tx_compromiso ) ? $commitment->tx_compromiso : null,
            'solution_date'           => isset( $commitment->d_fecha ) ? $commitment->d_fecha : null,
            'initial_time'            => isset( $commitment->t_hora_ini ) ? $commitment->t_hora_ini : null,
            'end_time'                => isset( $commitment->t_hora_fin ) ? $commitment->t_hora_fin : null,
            'solution'                => isset( $commitment->tx_solucion ) ? $commitment->tx_solucion : null,
            'solution_register_date'  => isset( $commitment->dt_solucion ) ? $commitment->dt_solucion : null,
            'status'                  => isset( $commitment->i_estado ) ? (bool) $commitment->i_estado : null,
            'created_at'              => isset( $commitment->created_at ) ? $commitment->created_at->format('Y-m-d H:i:s') : null,
        ];
    }
}