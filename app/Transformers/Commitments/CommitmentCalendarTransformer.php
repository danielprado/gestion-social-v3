<?php


namespace App\Transformers\Commitments;


use App\Commitments;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CommitmentCalendarTransformer extends TransformerAbstract
{
    public function transform(Commitments $commitments)
    {
        return [
            'id'                => isset( $commitments->i_pk_id ) ? $commitments->i_pk_id : null,
            'programming_id'    => isset( $commitments->i_fk_id_programacion ) ? $commitments->i_fk_id_programacion : null,
            'title'             => isset( $commitments->i_fk_id_programacion) ? "COMPROMISO DE LA ACTIVIDAD $commitments->i_fk_id_programacion" : null,
            'start'             => isset( $commitments->d_fecha, $commitments->t_hora_ini ) ? $commitments->d_fecha.' '.$commitments->t_hora_ini : null,
            'end'               => isset( $commitments->d_fecha, $commitments->t_hora_fin ) ? $commitments->d_fecha.' '.$commitments->t_hora_fin : null,
            'allDay'            => false,
            'commitment'        => true,
            'className'         =>  $this->getClassName( $commitments->d_fecha )
        ];
    }

    public function getClassName( $date = null )
    {
        if ( isset( $date ) ) {
            $date = Carbon::parse( $date );

            if ( Carbon::now()->greaterThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-azure';
            }

            if ( Carbon::now()->lessThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-green';
            }

            if ( Carbon::now()->format('Y-m-d') == $date->format('Y-m-d') ) {
                return 'event-orange';
            }
        }

        return 'event-red';
    }
}