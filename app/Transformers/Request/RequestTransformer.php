<?php


namespace App\Transformers\Request;


use App\Request;
use League\Fractal\TransformerAbstract;

class RequestTransformer extends TransformerAbstract
{
    public function transform(Request $request)
    {
        return [
            'id'    =>  isset( $request->i_pk_id )   ? (int) $request->i_pk_id : null,
            'name'  =>  isset( $request->vc_nombre ) ? $request->vc_nombre : null,
        ];
    }
}