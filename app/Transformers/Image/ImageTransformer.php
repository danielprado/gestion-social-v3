<?php


namespace App\Transformers\Image;


use App\Images;
use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    public function transform(Images $image)
    {
        return [
            'id'                =>  isset( $image->i_pk_id ) ? $image->i_pk_id : 0,
            'programming_id'    =>  isset( $image->i_fk_id_programacion ) ? $image->i_fk_id_programacion : null,
            'name'              =>  isset( $image->vc_file_name ) ? $image->vc_file_name : null,
            'description'       =>  isset( $image->tx_descripcion ) ? $image->tx_descripcion : null,
            'related'           =>  isset( $image->bl_is_related ) &&  (bool) $image->bl_is_related ? 'check' : 'cancel' ,
            'store'             =>  isset($image->vc_path)
                                    ? file_exists(public_path('/storage/images/'.$image->vc_path)) ? asset( 'public/storage/images/'.$image->vc_path )
                                    : asset('public/img/img_404.png') : asset('public/img/img_404.png'),
            'created_at'        =>  isset( $image->created_at ) ? $image->created_at->format('Y-m-d H:i:s') : null,
        ];
    }
}