<?php


namespace App\Transformers\FileType;


use App\FileType;
use League\Fractal\TransformerAbstract;

class FilleTypeTransformer extends TransformerAbstract
{
    public function transform( FileType $type ) {
        return [
            'id'    =>  isset( $type->id ) ? $type->id : null,
            'name'  =>  isset( $type->file_type ) ? $type->file_type : null,
        ];
    }
}