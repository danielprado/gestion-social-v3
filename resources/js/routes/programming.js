import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
const Programming = () =>
  import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/Programming.vue");
const ProgrammingView = () =>
  import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/Schedule.vue");
const ProgrammingTable = () =>
    import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/ProgrammingTable.vue");
const CanceledProgrammingTable = () =>
    import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/CanceledProgrammingTable.vue");
const NoAgreementProgrammingTable = () =>
    import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/NoAgreementProgrammingTable.vue");
const NoExecutionProgrammingTable = () =>
    import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/NoExecutionProgrammingTable.vue");
const Execution = () =>
    import(/* webpackChunkName: "programming" */ "@/pages/Dashboard/Programming/Execution.vue");
const Report = () =>
    import(/* webpackChunkName: "report" */ "@/pages/Dashboard/Programming/Report.vue");

const programming = {
  path: "/programming",
  component: DashboardLayout,
  redirect: "/programming/create",
  name: "Programming",
  children: [
    {
      path: "create",
      name: "Create Programming",
      components: { default: Programming },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['create_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "report",
      name: "Report",
      components: { default: Report },
      meta: {
        requiresAuth: true,
        can: true //Vue.auth.can(['process_report', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "my-schedules",
      name: "My Schedules",
      components: { default: ProgrammingTable },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['my_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "my-schedules-canceled",
      name: "My Schedules Canceled",
      components: { default: CanceledProgrammingTable },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['my_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "my-schedules-without-execution",
      name: "My Schedules Without Execution",
      components: { default: NoExecutionProgrammingTable },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['my_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "my-schedules-without-agreements",
      name: "My Schedules Without Agreements",
      components: { default: NoAgreementProgrammingTable },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['my_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/execution",
      name: "Execution",
      components: { default: Execution },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['record_execution', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/details",
      name: "Details",
      components: { default: ProgrammingView },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['my_schedules', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    }
  ],
};

export default programming;
