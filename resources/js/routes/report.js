import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
const Report = () =>
    import(/* webpackChunkName: "report" */ "@/pages/Dashboard/Programming/Report.vue");

const report = {
  path: "/report",
  component: DashboardLayout,
  redirect: "/report/general",
  children: [
    {
      path: "general",
      name: "General Report",
      components: { default: Report },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can(['process_report', 'admin']),
        //rtlActive: Vue.auth.isRTL
      }
    }
  ],
};

export default report;
