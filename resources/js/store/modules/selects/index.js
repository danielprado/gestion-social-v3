import actions from '@/store/modules/selects/actions';
import getters from '@/store/modules/selects/getters';
import mutations from '@/store/modules/selects/mutations';

const state = {
    activities: null,
    city: null,
    country: null,
    document: null,
    eps: null,
    location: null,
    supercade: null,
};

export default { actions, mutations, getters, state }

