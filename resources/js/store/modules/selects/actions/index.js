const actions = {
    initial_data_selects: ({commit}, response) => {
        commit('INITIAL_DATA', response);
    },
    set_activities: ({commit}, response) => {
        commit('SET_ACTIVITIES', response)
    },
    set_city: ({commit}, response) => {
        commit('SET_CITY', response)
    },
    set_country: ({commit}, response) => {
        commit('SET_COUNTRY', response)
    },
    set_document: ({commit}, response) => {
        commit('SET_DOCUMENT', response)
    },
    set_eps: ({commit}, response) => {
        commit('SET_EPS', response)
    },
    set_location: ({commit}, response) => {
        commit('SET_LOCATION', response)
    },
    set_supercade: ({commit}, response) => {
        commit('SET_SUPERCADE', response)
    },
    reset_activities: ({commit}) => {
        commit('RESET_ACTIVITIES');
    },
    reset_city: ({commit}) => {
        commit('RESET_CITY');
    },
    reset_country: ({commit}) => {
        commit('RESET_COUNTRY');
    },
    reset_document: ({commit}) => {
        commit('RESET_DOCUMENT');
    },
    reset_eps: ({commit}) => {
        commit('RESET_EPS');
    },
    reset_location: ({commit}) => {
        commit('RESET_LOCATION');
    },
    reset_supercade: ({commit}) => {
        commit('RESET_SUPERCADE');
    },
    reset_all_data: ({commit}) => {
        commit('RESET_ALL_DATA');
    }
};

export default actions;