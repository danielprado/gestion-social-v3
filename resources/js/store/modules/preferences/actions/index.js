const actions = {
    set_accent: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('ACCENT', response);
            resolve();
        })
    },
    set_background_color: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('BGCOLOR', response);
            resolve();
        })
    },
    set_mini: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('sidebarMini', response);
            resolve();
        })
    },
    set_image: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('sidebarImg', response);
            resolve();
        })
    },
    set_lang: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('LANG', response);
            resolve();
        })
    },
    set_background_image: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('BGIMAGE', response);
            resolve();
        })
    },
    set_rtl: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('RTL', response);
            resolve();
        })
    },
    
};

export default actions;