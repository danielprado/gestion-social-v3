const mutations = {
    COUNTERS( state, response ) {
        state.counter_total   = response.data.total;
        state.counter_renew   = response.data.renew;
        state.counter_today   = response.data.today;
        state.counter_month   = response.data.month;
    },
    RESET(state) {
        state.counter_total = null;
        state.counter_renew = null;
        state.counter_today = null;
        state.counter_month = null;
    },
};

export default mutations;