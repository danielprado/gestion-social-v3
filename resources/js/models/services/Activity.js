import { Model } from "../Model";
import { Api } from "../Api"

export class Activity extends Model {
    constructor () {
        super( Api.END_POINTS.ACTIVITY() , {} );
    }
}