import { Model } from "../Model";
import { Api } from "../Api"

export class Commitment extends Model {
    constructor ( id ) {
        super( Api.END_POINTS.COMMITMENTS(id) , {
            request: 'commitment',
            responsable: null,
            commitment_description: null,
            expired_at: null,
            initial_time: null,
            end_time: null
        });
        this.validations = {
            required: {
                required: true,
            },
            date_format: {
                required: true,
                date_format: 'yyyy-MM-dd'
            },
            hour_format: {
                required: true,
                date_format: 'HH:mm',
                before: 'end_time'
            },
            hour_format_end: {
                required: true,
                date_format: 'HH:mm',
            },
        };
        this.touched = {
            responsable: false,
            commitment_description: false,
            expired_at: false,
            initial_time: false,
            end_time: false,
        }
    }

    store(id, options = {}) {
        return this.post( Api.END_POINTS.EXECUTION(id), options );
    }
    
    calendar( options = null ) {
        return this.get( `${ Api.END_POINTS.CALENDAR_COMMIT() }`, options );
    }
}