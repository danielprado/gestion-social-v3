import { Model } from "../Model";
import { Api } from "../Api"

export class Solution extends Model {
    constructor (commitment_id ) {
        super( Api.END_POINTS.SOLUTION(commitment_id) , {
            request: 'solution',
            solution_description: null,
            solved_at: null
        });
        this.validations = {
            required: {
                required: true,
            },
            date_format: {
                required: true,
                date_format: 'yyyy-MM-dd'
            }
        };
        this.touched = {
            solution_description: false,
            solved_at: false,
        };
    }

}