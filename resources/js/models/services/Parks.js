import { Model } from "../Model";
import { Api } from "../Api"

export class Parks extends Model {
    constructor ( data ) {
        super( Api.END_POINTS.PARKS() , data );
    }

    finder(query) {
        return this.get( `${Api.END_POINTS.PARKS()}/finder`, { params: {query: query} } )
    }

    programmed(options = {}) {
        return this.get( `${Api.END_POINTS.PARKS()}/programmed`, options )
    }
}