import { Model } from "../Model";
import { Api } from "../Api"

export class Process extends Model {
    constructor () {
        super( Api.END_POINTS.PROCESS() , {} );
    }
}