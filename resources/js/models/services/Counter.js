import { Model } from "../Model";
import { Api } from "../Api"

export class Counter extends Model {
  constructor () {
    super( Api.END_POINTS.COUNTERS() , {} );
  }
}