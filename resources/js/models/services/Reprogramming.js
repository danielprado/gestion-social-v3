import { Model } from "../Model";
import { Api } from "../Api"
export class Reprogamming extends Model {
    constructor () {
        super( Api.END_POINTS.PROGRAMMING() ,{
            execution_date: null,
            initial_time: null,
            end_time: null
        });
        this.validations = {
            required: {
                required: true
            },
            number: {
                required: true,
                numeric: true
            },
            hour_format: {
                required: true,
                date_format: 'HH:mm',
                before: 'end_time'
            },
            hour_format_end: {
                required: true,
                date_format: 'HH:mm',
            },
            date_format: {
                required: true,
                date_format: 'yyyy-MM-dd'
            }
        };
        this.touched = {
            execution_date: false,
            initial_time: false,
            end_time: false
        };
    }
}