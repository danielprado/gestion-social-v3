import { Model } from "../Model";
import { Api } from "../Api"

export class Execution extends Model {
    constructor ( id ) {
        super( Api.END_POINTS.EXECUTION(id) , {
            request: null,
            entity: null,
            type: null,
            condition: null,
            situation: null,
            age_0_5_f: 0,
            age_0_5_m: 0,
            age_6_12_f: 0,
            age_6_12_m: 0,
            age_13_17_f: 0,
            age_13_17_m: 0,
            age_18_26_f: 0,
            age_18_26_m: 0,
            age_27_59_f: 0,
            age_27_59_m: 0,
            age_60_f: 0,
            age_60_m: 0,
        });
        this.validations = {
            required: {
                required: true,
                numeric: true
            },
            numeric: {
                numeric: true,
            }
        };
        this.touched = {
            entity: false,
            type: false,
            condition: false,
            situation: false,
            age_0_5_f: false,
            age_0_5_m: false,
            age_6_12_f: false,
            age_6_12_m: false,
            age_13_17_f: false,
            age_13_17_m: false,
            age_18_26_f: false,
            age_18_26_m: false,
            age_27_59_f: false,
            age_27_59_m: false,
            age_60_f: false,
            age_60_m: false,
        }
    }
}