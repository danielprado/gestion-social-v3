import { Model } from "../Model";
import { Api } from "../Api"
export class CancelProgramming extends Model {
  constructor ( id, data ) {
    super( `${ Api.END_POINTS.CANCEL_PROGRAMMING(id) }`, data);
  }
}