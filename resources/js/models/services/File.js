import { Model } from "../Model";
import { Api } from "../Api"

export class File extends Model {
    constructor ( id ) {
        super( Api.END_POINTS.FILE(id) , {
            file: null,
            file_type: null,
        });
        this.validation = {
            required: {
                required: true,
            },
        };
        this.touched = {
            file: false,
            file_type: false,
        }
    }
    
    delete(options = {}) {
        return super.delete(`${this.url}/delete`, options);
    }
}