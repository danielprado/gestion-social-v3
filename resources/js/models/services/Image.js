import { Model } from "../Model";
import { Api } from "../Api"

export class Image extends Model {
    constructor ( id ) {
        super( Api.END_POINTS.IMAGES(id) , {
            request: 'images',
            image_1: null,
            confirm_1: false,
            description_1: null,
        });
        this.validations = {
            image: {
                image: true,
                required: true,
            },
            required: {
                required: true,
            }
        };
        this.touched = {
            description_1: false,
        }
    }

    upload( id ) {
        return this.post( Api.END_POINTS.EXECUTION(id), {
            header :{
                'Content-Type' : 'multipart/form-data'
            }
        })
    }
}