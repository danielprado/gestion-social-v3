import { Model } from "../Model";
import { Api } from "../Api"

export class Professional extends Model {
  constructor () {
    super( Api.END_POINTS.PROFESSIONALS() , {} );
  }
}