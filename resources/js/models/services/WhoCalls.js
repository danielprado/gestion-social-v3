import { Model } from "../Model";
import { Api } from "../Api"
export class WhoCalls extends Model {
    constructor () {
        super( Api.END_POINTS.WHO_CALLS() , {} );
    }
}