import { Model } from "../Model";
import { Api } from "../Api"

export class Request extends Model {
    constructor () {
        super( Api.END_POINTS.REQUEST() , {} );
    }
}