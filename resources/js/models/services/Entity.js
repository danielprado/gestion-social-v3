import { Model } from "../Model";
import { Api } from "../Api"

export class Entity extends Model {
    constructor () {
        super( Api.END_POINTS.ENTITIES() , {});
    }
}