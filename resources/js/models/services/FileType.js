import { Model } from "../Model";
import { Api } from "../Api"

export class FileType extends Model {
    constructor () {
        super( Api.END_POINTS.FILE_TYPES() , {} );
    }
}