import { Model } from "../Model";
import { Api } from "../Api"

export class Type extends Model {
  constructor() {
    super( Api.END_POINTS.TYPES(), {} )
  }
}