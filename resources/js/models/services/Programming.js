import { Model } from "../Model";
import { Api } from "../Api"
export class Programming extends Model {
    constructor () {
        super( Api.END_POINTS.PROGRAMMING() ,{
            execution_date: null,
            initial_time: null,
            end_time: null,
            type_of_meeting: null,
            attemption: null,
            park: null,
            place: null,
            upz: null,
            objective: null,
            who_calls: null,
            who_calls_text: null,
            request: null,
            process: null,
            activity: null,
            subactivity: null,
        });
        this.validations = {
            required: {
                required: true
            },
            number: {
                required: true,
                numeric: true
            },
            hour_format: {
                required: true,
                date_format: 'HH:mm',
                before: 'end_time'
            },
            hour_format_end: {
                required: true,
                date_format: 'HH:mm',
            },
            date_format: {
                required: true,
                date_format: 'yyyy-MM-dd'
            }
        };
        this.touched = {
            execution_date: false,
            reunion_type: false,
            attemption: false,
            initial_time: false,
            end_time: false,
            park: false,
            place: false,
            upz: false,
            objective: false,
            type_of_meeting: false,
            who_calls: false,
            request: false,
            process: false,
            activity: false,
            subactivity: false
        };
    }

    calendar( options = null ) {
        return this.get( `${ Api.END_POINTS.PROGRAMMING() }/calendar`, options );
    }
}