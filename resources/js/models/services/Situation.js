import { Model } from "../Model";
import { Api } from "../Api"

export class Situation extends Model {
    constructor () {
        super( Api.END_POINTS.SITUATIONS() , {} );
    }
}