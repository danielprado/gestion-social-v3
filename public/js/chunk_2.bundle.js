(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_element_loading__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-element-loading */ "./node_modules/vue-element-loading/lib/vue-element-loading.min.js");
/* harmony import */ var vue_element_loading__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_element_loading__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var epic_spinners__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! epic-spinners */ "./node_modules/epic-spinners/src/lib.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _models_services_FileType__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/models/services/FileType */ "./resources/js/models/services/FileType.js");
/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/models/Api */ "./resources/js/models/Api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DropzoneAgreement",
  components: {
    vueDropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_2___default.a,
    VueElementLoading: vue_element_loading__WEBPACK_IMPORTED_MODULE_0___default.a,
    AtomSpinner: epic_spinners__WEBPACK_IMPORTED_MODULE_1__["AtomSpinner"]
  },
  data: function data() {
    return {
      dropzone_file: {
        url: '',
        thumbnailWidth: 150,
        maxFilesize: 60,
        //MB
        addRemoveLinks: true,
        parallelUploads: 1,
        maxFiles: 1,
        autoProcessQueue: false,
        acceptedFiles: '.pdf',
        headers: {
          'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
        },
        dictDefaultMessage: '<i class="fas fa-cloud-upload-alt text-info fa-10x"></i><br><p>Arrastra o da click aquí para subir el acta.</p>'
      },
      file_types: [],
      file_types_disabled: true,
      file_type: null,
      file_count: 0,
      touched: {
        file_type: false
      }
    };
  },
  created: function created() {
    this.axiosInterceptor();

    if (this.$route.params.id) {
      this.dropzone_file.url = _models_Api__WEBPACK_IMPORTED_MODULE_5__["Api"].END_POINTS.EXECUTION(this.$route.params.id);
    }

    if (this.file_types.length < 1) {
      this.getFileTypes();
    }
  },
  methods: {
    getFileTypes: _.debounce(function () {
      var _this = this;

      this.file_types_disabled = true;
      new _models_services_FileType__WEBPACK_IMPORTED_MODULE_4__["FileType"]().index().then(function (response) {
        _this.file_types = response.data;
        _this.file_types_disabled = false;
      }).catch(function (error) {
        console.log(error);
      });
    }, 300),
    // Agreement File
    validate: function validate() {
      var _this2 = this;

      this.$validator.validateAll().then(function (isValid) {
        if (isValid && _this2.file_count === 1) {
          _this2.$refs.dropzone_agreement.processQueue();
        } else {
          _this2.$notify({
            message: _this2.$t('complete form') || _this2.unexpected,
            icon: "add_alert",
            horizontalAlign: 'center',
            verticalAlign: 'top',
            type: 'danger'
          });
        }
      });
    },
    onFile: function onFile(file) {
      if ('undefined' !== typeof this.$refs.dropzone_agreement.dropzone) {
        this.file_count = this.$refs.dropzone_agreement.dropzone.files.length;
      } else {
        this.file_count = 0;
      }
    },
    onMaxFiles: function onMaxFiles(file) {
      this.$refs.dropzone_agreement.removeFile(file);
    },
    onSending: function onSending(file, xhr, formData) {
      formData.append('request', 'agreement');
      formData.append('file_type', this.file_type);
    },
    onCancelUpload: function onCancelUpload(file) {
      this.$refs.dropzone_agreement.removeFile(file);
    },
    onSuccessUpload: function onSuccessUpload(file, response) {
      this.$refs.dropzone_agreement.removeFile(file);

      if (file.status === "success") {
        this.$emit('on-submit');
        this.$notify({
          message: response.data,
          icon: "add_alert",
          horizontalAlign: 'center',
          verticalAlign: 'top',
          type: 'success'
        });
      }
    },
    onErrorUpload: function onErrorUpload(file, message, xhr) {
      this.$refs.dropzone_agreement.removeFile(file);
      this.$notify({
        message: message.message || this.unexpected,
        icon: "add_alert",
        horizontalAlign: 'center',
        verticalAlign: 'top',
        type: 'danger'
      });
    }
  },
  watch: {
    file_type: function file_type() {
      this.touched.file_type = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "md-card",
    [
      _c("md-card-content", [
        _c(
          "div",
          { staticClass: "md-layout" },
          [
            _c(
              "vue-element-loading",
              { attrs: { active: _vm.isLoading } },
              [_c("atom-spinner", { attrs: { size: 100, color: "#00bcd4" } })],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "md-layout-item md-size-100 md-small-size-100 md-xsmall-size-100 mx-auto"
              },
              [
                _c(
                  "md-field",
                  {
                    class: [
                      {
                        "md-valid":
                          !_vm.errors.has(_vm.$t("file_type")) &&
                          _vm.touched.file_type
                      },
                      { "md-error": _vm.errors.has(_vm.$t("file_type")) }
                    ],
                    attrs: { "md-clearable": "" }
                  },
                  [
                    _c("md-icon", [_vm._v("description")]),
                    _vm._v(" "),
                    _c("label", { attrs: { for: "file_type" } }, [
                      _vm._v(_vm._s(_vm.$t("file_type").capitalize()))
                    ]),
                    _vm._v(" "),
                    _c(
                      "md-select",
                      {
                        directives: [
                          {
                            name: "validate",
                            rawName: "v-validate",
                            value: "required",
                            expression: "'required'"
                          }
                        ],
                        attrs: {
                          disabled: _vm.file_types_disabled,
                          "data-vv-name": _vm.$t("file_type"),
                          required: "",
                          name: "file_type",
                          id: "file_type"
                        },
                        model: {
                          value: _vm.file_type,
                          callback: function($$v) {
                            _vm.file_type = _vm._n($$v)
                          },
                          expression: "file_type"
                        }
                      },
                      _vm._l(_vm.file_types, function(item) {
                        return _c(
                          "md-option",
                          { key: item.id, attrs: { value: item.id } },
                          [_vm._v(_vm._s(item.name))]
                        )
                      }),
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "md-layout-item md-size-100 md-small-size-100 md-xsmall-size-100 mx-auto"
              },
              [
                _c("vue-dropzone", {
                  ref: "dropzone_agreement",
                  attrs: { id: "agreement", options: _vm.dropzone_file },
                  on: {
                    "vdropzone-file-added": _vm.onFile,
                    "vdropzone-max-files-exceeded": _vm.onMaxFiles,
                    "vdropzone-sending": _vm.onSending,
                    "vdropzone-removed-file": _vm.onFile,
                    "vdropzone-canceled": _vm.onCancelUpload,
                    "vdropzone-success": _vm.onSuccessUpload,
                    "vdropzone-error": _vm.onErrorUpload
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "md-card-actions",
        { staticClass: "text-center" },
        [
          _c(
            "md-button",
            {
              staticClass: "md-info",
              attrs: { "native-type": "submit", disabled: _vm.isLoading },
              nativeOn: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.validate($event)
                }
              }
            },
            [_vm._v(_vm._s(_vm.$t("Upload File")))]
          ),
          _vm._v(" "),
          _vm._t("default")
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/models/services/FileType.js":
/*!**************************************************!*\
  !*** ./resources/js/models/services/FileType.js ***!
  \**************************************************/
/*! exports provided: FileType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileType", function() { return FileType; });
/* harmony import */ var _Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var _Model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Model */ "./resources/js/models/Model.js");
/* harmony import */ var _Api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Api */ "./resources/js/models/Api.js");






var FileType =
/*#__PURE__*/
function (_Model) {
  Object(_Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(FileType, _Model);

  function FileType() {
    Object(_Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, FileType);

    return Object(_Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Object(_Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_2__["default"])(FileType).call(this, _Api__WEBPACK_IMPORTED_MODULE_5__["Api"].END_POINTS.FILE_TYPES(), {}));
  }

  return FileType;
}(_Model__WEBPACK_IMPORTED_MODULE_4__["Model"]);

/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue":
/*!**************************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true& */ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true&");
/* harmony import */ var _DropzoneAgreement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DropzoneAgreement.vue?vue&type=script&lang=js& */ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DropzoneAgreement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "582a93dc",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneAgreement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DropzoneAgreement.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneAgreement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true&":
/*!*********************************************************************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true& ***!
  \*********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneAgreement.vue?vue&type=template&id=582a93dc&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneAgreement_vue_vue_type_template_id_582a93dc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);