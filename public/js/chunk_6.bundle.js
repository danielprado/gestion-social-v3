(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es6.function.name */ "./node_modules/core-js/modules/es6.function.name.js");
/* harmony import */ var core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es6_function_name__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var vue_element_loading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-element-loading */ "./node_modules/vue-element-loading/lib/vue-element-loading.min.js");
/* harmony import */ var vue_element_loading__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_element_loading__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var epic_spinners__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! epic-spinners */ "./node_modules/epic-spinners/src/lib.js");
/* harmony import */ var _models_services_Image__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/models/services/Image */ "./resources/js/models/services/Image.js");
/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/models/Api */ "./resources/js/models/Api.js");




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DropzoneImages",
  components: {
    VueElementLoading: vue_element_loading__WEBPACK_IMPORTED_MODULE_4___default.a,
    AtomSpinner: epic_spinners__WEBPACK_IMPORTED_MODULE_5__["AtomSpinner"]
  },
  data: function data() {
    return {
      form: null,
      image_1: null
    };
  },
  created: function created() {
    this.axiosInterceptor();

    if (this.$route.params.id) {
      this.form = new _models_services_Image__WEBPACK_IMPORTED_MODULE_6__["Image"](this.$route.params.id);
    }
  },
  methods: {
    validate: function () {
      var _validate = Object(_Users_danielprado_Sites_gestion_social_v3_node_modules_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _this = this;

        var image_1;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.image_1.promisedBlob();

              case 2:
                image_1 = _context.sent;
                this.$validator.validateAll().then(function (isValid) {
                  if (isValid && _this.image_1.hasImage()) {
                    _this.form.image_1 = image_1;
                    var form_data = new FormData();
                    form_data.append('request', 'images');
                    form_data.append('image_1', _this.form.image_1, _this.image_1.getChosenFile().name);
                    form_data.append('description_1', _this.form.description_1);
                    form_data.append('confirm_1', _this.form.confirm_1);
                    var config = {
                      header: {
                        'Content-Type': 'multipart/form-data'
                      }
                    };
                    window.axios.post(_models_Api__WEBPACK_IMPORTED_MODULE_7__["Api"].END_POINTS.EXECUTION(_this.$route.params.id), form_data, config).then(function (response) {
                      _this.$emit('on-submit');

                      _this.clear();

                      _this.$notify({
                        message: response.data.data,
                        icon: "add_alert",
                        horizontalAlign: 'center',
                        verticalAlign: 'top',
                        type: 'success'
                      });
                    }).catch(function (error) {
                      _this.$notify({
                        message: error.message || _this.unexpected,
                        icon: "add_alert",
                        horizontalAlign: 'center',
                        verticalAlign: 'top',
                        type: 'danger'
                      });
                    });
                  } else {
                    var _form_data = new FormData();

                    _form_data.append('request', 'images');

                    _form_data.append('description_1', _this.form.description_1);

                    _form_data.append('confirm_1', _this.form.confirm_1);

                    var _config = {
                      header: {
                        'Content-Type': 'multipart/form-data'
                      }
                    };
                    window.axios.post(_models_Api__WEBPACK_IMPORTED_MODULE_7__["Api"].END_POINTS.EXECUTION(_this.$route.params.id), _form_data, _config).then(function (response) {
                      _this.$emit('on-submit');

                      _this.clear();

                      _this.$notify({
                        message: response.data.data,
                        icon: "add_alert",
                        horizontalAlign: 'center',
                        verticalAlign: 'top',
                        type: 'success'
                      });
                    }).catch(function (error) {
                      _this.$notify({
                        message: error.message || _this.unexpected,
                        icon: "add_alert",
                        horizontalAlign: 'center',
                        verticalAlign: 'top',
                        type: 'danger'
                      });
                    });
                  }
                });

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function validate() {
        return _validate.apply(this, arguments);
      }

      return validate;
    }(),
    clear: function clear() {
      this.image_1.remove();
      this.form.reset();
    }
  },
  watch: {
    'form.description_1': function formDescription_1() {
      this.form.touched.description_1 = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "md-card",
    [
      _c("md-card-content", [
        _c(
          "div",
          { staticClass: "md-layout" },
          [
            _c(
              "vue-element-loading",
              { attrs: { active: _vm.isLoading } },
              [_c("atom-spinner", { attrs: { size: 100, color: "#00bcd4" } })],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "md-layout-item md-xsmall-size-100 md-small-size-100 md-size-50 mx-auto"
              },
              [
                _c("h4", { staticClass: "card-title" }),
                _vm._v(" "),
                _c("croppa", {
                  attrs: {
                    width: 200,
                    height: 200,
                    placeholder: _vm.$t("Click here to upload image"),
                    "placeholder-font-size": 16,
                    "prevent-white-space": true,
                    "show-remove-button": true,
                    "remove-button-color": "black",
                    "remove-button-size": 50,
                    "show-loading": true,
                    quality: 5,
                    "loading-size": 50
                  },
                  model: {
                    value: _vm.image_1,
                    callback: function($$v) {
                      _vm.image_1 = $$v
                    },
                    expression: "image_1"
                  }
                }),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c(
                  "md-button",
                  {
                    staticClass: "md-simple md-just-icon",
                    on: {
                      click: function($event) {
                        return _vm.image_1.rotate()
                      }
                    }
                  },
                  [_c("md-icon", [_vm._v("crop_rotate")])],
                  1
                ),
                _vm._v(" "),
                _c(
                  "md-button",
                  {
                    staticClass: "md-simple md-just-icon",
                    on: {
                      click: function($event) {
                        return _vm.image_1.flipY()
                      }
                    }
                  },
                  [_c("md-icon", [_vm._v("flip")])],
                  1
                ),
                _vm._v(" "),
                _c(
                  "md-button",
                  {
                    staticClass: "md-simple md-just-icon",
                    on: {
                      click: function($event) {
                        return _vm.image_1.flipX()
                      }
                    }
                  },
                  [_c("md-icon", [_vm._v("flip")])],
                  1
                ),
                _vm._v(" "),
                _c(
                  "md-button",
                  {
                    staticClass: "md-simple md-just-icon",
                    on: {
                      click: function($event) {
                        return _vm.image_1.zoomIn()
                      }
                    }
                  },
                  [_c("md-icon", [_vm._v("zoom_in")])],
                  1
                ),
                _vm._v(" "),
                _c(
                  "md-button",
                  {
                    staticClass: "md-simple md-just-icon",
                    on: {
                      click: function($event) {
                        return _vm.image_1.zoomOut()
                      }
                    }
                  },
                  [_c("md-icon", [_vm._v("zoom_out")])],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "md-layout-item md-xsmall-size-100 md-small-size-100 md-size-50"
              },
              [
                _c("h4", { staticClass: "card-title" }),
                _vm._v(" "),
                _c(
                  "md-field",
                  {
                    class: [
                      {
                        "md-valid":
                          !_vm.errors.has(_vm.$t("description") + "_1") &&
                          _vm.form.touched.description_1
                      },
                      {
                        "md-error":
                          _vm.errors.has(_vm.$t("objective")) ||
                          _vm.form.errors.has("description_1")
                      }
                    ],
                    attrs: { "md-clearable": "" }
                  },
                  [
                    _c("label", { attrs: { for: "description_1" } }, [
                      _vm._v(_vm._s(_vm.$t("description").capitalize()))
                    ]),
                    _vm._v(" "),
                    _c("md-textarea", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: _vm.form.validations.required,
                          expression: "form.validations.required"
                        }
                      ],
                      attrs: {
                        name: "description_1",
                        id: "description_1",
                        "data-vv-name": _vm.$t("description") + "_1",
                        type: "text",
                        required: "",
                        maxlength: "2500",
                        autocomplete: "off"
                      },
                      model: {
                        value: _vm.form.description_1,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "description_1", $$v)
                        },
                        expression: "form.description_1"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "md-switch",
                  {
                    model: {
                      value: _vm.form.confirm_1,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "confirm_1", $$v)
                      },
                      expression: "form.confirm_1"
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("confirm image")))]
                )
              ],
              1
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "md-card-actions",
        { staticClass: "text-center" },
        [
          _c(
            "md-button",
            {
              staticClass: "md-info",
              attrs: { "native-type": "submit", disabled: _vm.isLoading },
              nativeOn: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.validate($event)
                }
              }
            },
            [_vm._v(_vm._s(_vm.$t("Upload Images")))]
          ),
          _vm._v(" "),
          _vm._t("default")
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue":
/*!***********************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true& */ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true&");
/* harmony import */ var _DropzoneImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DropzoneImages.vue?vue&type=script&lang=js& */ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DropzoneImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "248f88a0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DropzoneImages.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true& ***!
  \******************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Programming/ExecutionForm/DropzoneImages.vue?vue&type=template&id=248f88a0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropzoneImages_vue_vue_type_template_id_248f88a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);